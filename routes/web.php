<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Mail\Email;
 
Route::get('/email', function () {
 
   Mail::to('admin@luxodev.com')->send(new Email);
 
return view('emails.email');
 
});

Route::get('/', function () {
    return redirect('/login');
});

Route::get('/login','Auth\LoginController@showLoginForm')->name('login');
Route::post('/login','Auth\LoginController@login');

// Route::get('test', function () {
//     event(new App\Events\CustomerOrder());
//     return "Event has been sent!";
// });

Route::Group(['middleware'=>'App\Http\Middleware\AdminMiddleware'],function(){
    Route::resource('/currencies', 'CurrencyController');
    Route::resource('/currencies.detail_currencies', 'CurrencyDetailCurrencyController');
    Route::resource('/customers', 'CustomerController');
    Route::resource('/reports', 'ReportController');
    Route::resource('/settings', 'SettingController');
    Route::get('/reports_view','ReportController@index2')->name('reports_view.index');
    Route::patch('/order_delivery/{order}','OrderController@update_delivery')->name('orders.update_delivery');
    Route::resource('/rates', 'RateController');
    Route::resource('/couriers', 'CourierController');
    Route::resource('/orders', 'OrderController');
    Route::resource('/users', 'UserController');
});

Route::Group(['middleware'=>'auth'],function(){
    Route::post('/logout','Auth\LoginController@logout')->name('logout');
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/rates', 'RateController@index')->name('rates.index');
    Route::get('/couriers', 'CourierController@index')->name('couriers.index');
    Route::get('/orders', 'OrderController@index')->name('orders.index');
});