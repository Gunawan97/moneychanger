<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware'=>'App\Http\Middleware\CheckAuthToken'], function(){
    Route::prefix('customer')->group(function () {
        Route::post('login', 'Api\LoginApiController@getCustomerLoginData'); 
        Route::get('profile/{customer_id}', 'Api\ProfileApiController@getCustomerProfileData');
        Route::get('login/approval/{customer_id}', 'Api\LoginApiController@cekApproval');
        
        Route::get('history', 'Api\OrderApiController@getOrderHistoryList');
        
    
        Route::get('currency', 'Api\CurrencyApiController@getCurrencyList');

        Route::post('order', 'Api\OrderApiController@processCreateOrder');
        Route::post('register', 'Api\RegisterApiController@RegisterCustomer');
        Route::post('register/otp', 'Api\RegisterApiController@sendOTPCode');

        Route::get('order/{order_id}', 'Api\OrderApiController@getDetailOrderList');
        Route::get('currency/{customer_id}', 'Api\CurrencyApiController@getDetailCurrencyList');
        Route::get('history/{order_id}', 'Api\OrderApiController@getDetailHistory');
        

    });

    Route::prefix('kurir')->group(function () {
        Route::post('login', 'Api\LoginApiController@getCourierLoginData');
        Route::get('login/status/{username}', 'Api\LoginApiController@cekStatus');
        Route::get('shipping', 'Api\ShippingApiController@getShippingList');
        Route::get('shipping/{order_id}', 'Api\ShippingApiController@getDetailShipping');
        Route::post('shipping/{order_id}','Api\ShippingApiController@processUpdateConfirmationShipping');
    
    });

});
