# DOLLAR EXPRESS

## API Documentation

### Customer Login

/api/customer/login

Header:
Content-type: application/json
Authorization: Wg6RdO04TD0DDoeb

Type:post

Request:{imei:"123sadfasf", customer_id:"1"}
Response:{
    -Berhasil Login: Status Code: 200
    -Customer Suspended: Status Code: 462 
    -Delete SP Pada HP: Status Code: 460

}


### Get Customer Profile Data

/api/customer/profile/{customer_id}

Header:
Content-type: application/json
Authorization: Wg6RdO04TD0DDoeb

Type:post

Request:{hash: "aawsdkhoi122" -> Hashing Dari Image}
Response:{
    -Hashing Image tidak sama dengan Image Database
        'customer'=>[
            'name'=> $customer->name,
            'phone'=> $customer->phone,
            'card_id' =>$customer->card_id,
            'address' =>$customer->address,
            'card_image' =>$customer->card_image,
        ],Status Code= 200

    -Hashing Image Sama dengan Image Database
        'customer'=>[
            'name'=> $customer->name,
            'phone'=> $customer->phone,
            'card_id' =>$customer->card_id,
            'address' =>$customer->address,
        ],Status Code=274
}


### Cek Customer Approval Status

/api/customer/login/approval/{customer_id}

Header:
Content-type: application/json
Authorization: Wg6RdO04TD0DDoeb

Type:get

Request:{}
Response:{
    -Customer Belum DiApprove
        Status Code= 461

    -Customer Sudah DiApprove
        Status Code=200
    
    -Customer Suspended
        Status Code=462
}


### Get Customer Order History List

/api/customer/history

Header:
Content-type: application/json
Authorization: Wg6RdO04TD0DDoeb

Type:get

Request:{
    status: 0 (Get Order History Yang Belum Selesai), 1 (Get Order History Yang Sudah Selesai / Dibatalkan),
    customer_id: "1"
}
Response:{listHistoryOrder (Order Type)}


### Get Currency List

/api/customer/currency

Header:
Content-type: application/json
Authorization: Wg6RdO04TD0DDoeb

Type:get

Request:{}
Response:{currencyList (Currency Type) -> Apabila Kosong Berarti Toko Tutup}


### Create Order Customer

/api/customer/order

Header:
Content-type: application/json
Authorization: Wg6RdO04TD0DDoeb

Type:post

Request:{
    currency_log_id:"4&7&8" ->Dipisah Dengan &,
    amount:"3&10&12" -> Dipisah Dengan &,
    customer_id:"1",
    total:"3000",
    delivery:"1" / "0"
}
Response:{
    -Jika Berhasil 
        'order' =>[
            'id'=>$createOrder->id,
            'order_number'=>$createOrder->order_number,
            
        ]
    -Terjadi Kesalahan Pada Saat Insert Detail Order dan Update CurrencyLog
        Status Code= 468
    -Total Yang Dikirimkan User Tidak Sesuai Dengan Perhitungan Total di Database
        Status Code= 467
    -Terdapat Order Yang Masih Pending
        Status Code= 474
    -Toko Tutup
        Status C0de= 472
    -Stok Barang Di Database Tidak Mencukupi
        Status Code= 466
            
}


### Register Customer

/api/customer/register

Header:
Content-type: application/json
Authorization: Wg6RdO04TD0DDoeb

Type:post

Request:{
    card_id:"12300011399221",
    phone:"084772342342",
    imei:"aldjasdf123",
    name:"Dino",
    address:"Siwalankerto 16"
}
Response:{
    -Jika Berhasil Register
        customer(Customer Type) 
    -Terjadi Kesalahan Saat Insert Data Customer
        Status Code= 465
    -Delete SP pada Hp
        Status Code= 460
    -No Hp Telah Digunakan
        Status Code= 470
    -Terjadi Kesalahan Saat Update Imei
        Status Code=464
}


### Send OTP Code

/api/customer/register/otp

Header:
Content-type: application/json
Authorization: Wg6RdO04TD0DDoeb

Type:post

Request:{phone:"089234234234"}
Response:{otp_code:"4773"}


### Get Detail Currency

/api/customer/currency/{customer_id}

Header:
Content-type: application/json
Authorization: Wg6RdO04TD0DDoeb

Type:get

Request:{}
Response:{
    currencyList ( Currency Tipe),
    detailCurrencyList (Detail Currency Type),
    maximumOrder:"123123"
}


### Get Detail History

/api/customer/history/{order_id}

Header:
Content-type: application/json
Authorization: Wg6RdO04TD0DDoeb

Type:get

Request:{}
Response:{
    order (Order Type),
    expiredDate (Date),
    accountName (String),
    accountNumber (Integer),
    remainingTime:"23 Minutes Remaining"
}


### Login Kurir

/api/kurir/login

Header:
Content-type: application/json

Type:post

Request:{username:"Tono", imei:"afsdf22323"}
Response:{
    -Jika Berhasil
        courierData (Courier Type),
    -Jika Status Kurir Suspended
        Status Code= 471
    -Jika Imei Beda / Tidak Ditemukan
        Status Code= 463
      
}


### Cek Status Kurir

/api/kurir/login/status/{username}

Header:
Content-type: application/json

Type:get

Request:{}
Response:{
    -Status Kurir Aktif
        Status Code= 200
    -Status Kurir Suspended
        Status Code= 471
    -Data Kurir Tidak Ditemukan
        Status Code= 473
      
}


### Get Shipping List Kurir

/api/kurir/shipping

Header:
Content-type: application/json

Type:get

Request:{courier_id:"1"}
Response:{
   'shippingList' =>[
        'orders.id',
        'orders.order_number',
        'customers.name',
        'customers.address' 

   ]
      
}


### Get Detail Shipping Kurir

/api/kurir/shipping/{order_id}

Header:
Content-type: application/json

Type:get

Request:{}
Response:{
    'shipping' =>[
        'id',
        'order_number',
        'name',
        'address',
        'total',
        'delivery_name',
        'status_name',
        'created_at',
        'date',
        'total_nominal'
    ],
    'detailShipping'=>[
        'detail_order_id',
        'orders.id',
        'currencies.code',
        'detail_currencies.nominal',
        'detail_orders.amount',
        'detail_orders.price',
        'detail_orders.total'
    ] 
      
}


### Update Confirmation Shipping Kurir

/api/kurir/shipping/{order_id}

Header:
Content-type: application/json

Type:post

Request:{
    receiver_name:"Tanu",
    image_receipt:"asdfja;lsdkf" (String Base64),
    latitude:"123123",
    longitude:"423434"

}
Response:{
    -Jika Berhasil
        Status Code= 200
    -Jika Terjadi Kesalahan Pada Saat Update Order
        Status Code= 469
}