<?php

namespace App\Http\Controllers;

use App\Currency;
use App\DetailCurrency;
use Illuminate\Http\Request;

class CurrencyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $currency = Currency::query();
        
        $filter = [];
        if(isset($request->filter)) {
            $filter = $request->filter;
            foreach ($filter as $key => $value) {
                if(!empty($value)) {
                    $currency = $currency->where($key, 'like', '%'.$value.'%');
                }
            }
        }
        
        $currency = $currency->paginate(50);
        return view('currencies.index',compact('currency','filter'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $currency = new Currency;
        return view('currencies.create',compact('currency'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $currency = new Currency;
        $currency->fill($request->all());
        $currency->save();
        $request->session()->flash('toast', 'Mata Uang berhasil ditambahkan!');
        return redirect('/currencies');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Currency  $currency
     * @return \Illuminate\Http\Response
     */
    public function show(Currency $currency)
    {
        $detail_currency = DetailCurrency::query();
        $detail_currency = $detail_currency->where('currency_id', '=', $currency->id);
        $detail_currency = $detail_currency->paginate(5, ['*'], 'nominal');
        return view('currencies.show',compact('currency','detail_currency'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Currency  $currency
     * @return \Illuminate\Http\Response
     */
    public function edit(Currency $currency)
    {
        return view('currencies.create',compact('currency'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Currency  $currency
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Currency $currency)
    {
        $currency->fill($request->all());
        $currency->save();
        $request->session()->flash('toast', 'Mata Uang berhasil diubah!');
        return redirect('/currencies');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Currency  $currency
     * @return \Illuminate\Http\Response
     */
    public function destroy(Currency $currency)
    {
        //
    }
}
