<?php

namespace App\Http\Controllers;
use App\Order;
use App\CustomerLog;
use Illuminate\Http\Request;
use Carbon\Carbon;
class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $order = Order::query();
        
        $filter = [];
        if(isset($request->filter)) {
            $filter = $request->filter;
            foreach ($filter as $key => $value) {

                if(!empty($value)) {
                    if($key=='date'){
                        $array=explode('-', $value);
                        $startdate=Carbon::createFromFormat('d/m/Y ', $array[0])->startOfDay();
                        $enddate=Carbon::createFromFormat(' d/m/Y', $array[1])->endOfDay();
                        $order = $order->where('created_at', '>=', $startdate)->where('created_at', '<=', $enddate);
                    }
                    else {
                        $order = $order->where($key, 'like', '%'.$value.'%');
                    }
                }


            }
        }
        else{
            $startdate=Carbon::now()->startOfDay();
            $enddate=Carbon::now()->endOfDay();
            $order = $order->where('created_at', '>=', $startdate)->where('created_at', '<=', $enddate);
        }
        $order = $order->paginate(50);
        $now = Carbon::now()->format('d/m/Y');
        return view('reports.index',compact('order','filter','now'));

    }

    public function index2(Request $request)
    {
        $customer_log = CustomerLog::query();
        
        $filter = [];
        if(isset($request->filter)) {
            $filter = $request->filter;
            foreach ($filter as $key => $value) {

                if(!empty($value)) {
                    if($key=='date'){
                        $array=explode('-', $value);
                        $startdate=Carbon::createFromFormat('d/m/Y ', $array[0])->startOfDay();
                        $enddate=Carbon::createFromFormat(' d/m/Y', $array[1])->endOfDay();
                        $customer_log = $customer_log->where('created_at', '>=', $startdate)->where('created_at', '<=', $enddate);
                    }
                    else {
                        $customer_log = $customer_log->where($key, 'like', '%'.$value.'%');
                    }
                }


            }
        }
        else{
            $startdate=Carbon::now()->startOfDay();
            $enddate=Carbon::now()->endOfDay();
            $customer_log = $customer_log->where('created_at', '>=', $startdate)->where('created_at', '<=', $enddate);
        }
        $customer_log = $customer_log->paginate(50);
        $now = Carbon::now()->format('d/m/Y');
        return view('reports.index2',compact('customer_log','filter','now'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
