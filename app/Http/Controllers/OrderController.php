<?php

namespace App\Http\Controllers;
use Auth;
use App\Order;
use App\Customer;
use App\Courier;
use App\HistoryChange;
use App\Setting;
use Illuminate\Http\Request;
use App\Events\CustomerOrder;
use Carbon\Carbon;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $order = Order::query();
        
        $filter = [];
        if(isset($request->filter)) {
            $filter = $request->filter;
            foreach ($filter as $key => $value) {
                if(!empty($value) && $key=='date') {
                    $date=Carbon::createFromFormat('Y-m-d', $value)->startOfDay();
                    $date2=Carbon::createFromFormat('Y-m-d', $value)->endOfDay();
                    $order = $order->where('created_at', '>=', $date)->where('created_at', '<=', $date2);
                }
                else if(!empty($value)) {
                    $order = $order->where($key, 'like', '%'.$value.'%');
                }
            }
        }
        $order = $order->orderBy('created_at','desc');
        $order = $order->paginate(50);
        $customer = Customer::pluck('name', 'id')->all();
        $expired = Setting::where('name','=','expiredtime')->first();
        return view('orders.index',compact('order','filter','customer','expired'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        $expired = Setting::where('name','=','expiredtime')->first();
        $courier = Courier::where('status','=',1)->pluck('username','id');
        return view('orders.show',compact('order','courier','expired'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        if($request->courier_id != NULL){//isi kurir
            $order->courier_id = $request->courier_id;
        }
        else if($request->receiver_name != NULL){
            $order->receiver_name = $request->receiver_name;
        }
        $order->status = $order->status +1;
        if ($order->status == 2 && $order->delivery == 2){
            $order->status = 3;
        }
        if($request->cancel == 1){//batalkan order        
            $order->restore();
            $order->status = 5;
        }
        event(new CustomerOrder());
        $order->save();
        $order->save_history("Admin",$order->status);
        return back();
    }

    public function update_delivery(Request $request, Order $order)
    {
        $order->delivery = 1+($order->delivery)%2;
        $order->save();
        $order->save_history("Admin",6);
        return back();
    }
    public function restoreStock($id){
        $order = Order::find($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }
}
