<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $setting=Setting::all();
        $open=new Carbon($setting[1]->value);
        $open=$open->format('g:i A');
        $close=new Carbon($setting[2]->value);
        $close=$close->format('g:i A');
        return view('settings.index',compact('setting','open','close'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function show(Setting $setting)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function edit(Setting $setting)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Setting $setting)
    {
        $setting=Setting::all();
        $setting[0]->value=$request->expiredtime;
        $setting[0]->save();
        $setting[1]->value=$request->opentime;
        $setting[1]->save();
        $setting[2]->value=$request->closetime;
        $setting[2]->save();
        $setting[3]->value=$request->account_number;
        $setting[3]->save();
        $setting[4]->value=$request->account_name;
        $setting[4]->save();
        $setting[5]->value=$request->max_transaction;
        $setting[5]->save();
        return redirect('/settings');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function destroy(Setting $setting)
    {
        //
    }
}
