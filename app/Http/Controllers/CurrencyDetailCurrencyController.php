<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Currency;
use App\DetailCurrency;
use App\CurrencyLog;
use Illuminate\Http\Request;

class CurrencyDetailCurrencyController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        //
    }
    
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create(Currency $currency)
    {
        $detail_currency = new DetailCurrency;
        return view('currency_detail_currencies.create',compact('detail_currency','currency'));
    }
    
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request, Currency $currency)
    {
        $a = $currency->id;
        Validator::make($request->all(), [
            'nominal' => Rule::unique('detail_currencies')->where(function ($query) use ($a) {
                return $query->where('currency_id', $a);
            })
         ])->validate();
        $detail_currency = new DetailCurrency;
        $detail_currency->fill($request->all());
        $detail_currency->currency_id=$currency->id;
        $detail_currency->save();
        $currency_log = new CurrencyLog;
        $currency_log->detail_currency_id = $detail_currency->id;
        $currency_log->stock = 0;
        $currency_log->price = 0;
        $currency_log->save();
        $request->session()->flash('toast','Pecahan Mata Uang berhasil ditambahkan!');
        return redirect('/currencies/'.$currency->id);
    }
    
    /**
    * Display the specified resource.
    *
    * @param  \App\DetailCurrency  $detailCurrency
    * @return \Illuminate\Http\Response
    */
    public function show(DetailCurrency $detail_currency)
    {
        //
    }
    
    /**
    * Show the form for editing the specified resource.
    *
    * @param  \App\DetailCurrency  $detailCurrency
    * @return \Illuminate\Http\Response
    */
    public function edit(Currency $currency, DetailCurrency $detail_currency)
    {
        return view('currency_detail_currencies.create',compact('detail_currency','currency'));
    }
    
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \App\DetailCurrency  $detailCurrency
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, Currency $currency, DetailCurrency $detail_currency)
    {
        $detail_currency->fill($request->all());
        $detail_currency->currency_id=$currency->id;
        $detail_currency->save();
        $request->session()->flash('toast','Pecahan Mata Uang berhasil diubah!');
        return redirect('/currencies/'.$currency->id);
    }
    
    /**
    * Remove the specified resource from storage.
    *
    * @param  \App\DetailCurrency  $detailCurrency
    * @return \Illuminate\Http\Response
    */
    public function destroy(Request $request, Currency $currency, DetailCurrency $detail_currency)
    {
        
        try{
            $currency_log = CurrencyLog::where('detail_currency_id','=',$detail_currency->id)->first();
            $currency_log->delete();
            $detail_currency->delete();
            $request->session()->flash('toast','Pecahan Mata Uang berhasil dihapus');
        }
        catch(\Illuminate\Database\QueryException $ex){
            $request->session()->flash('fail','Pecahan Mata Uang gagal dihapus '.substr($ex->getMessage(),0,15));
        };
        return redirect('/currencies/'.$currency->id);
    }
}
