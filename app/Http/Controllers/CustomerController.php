<?php

namespace App\Http\Controllers;

use App\Order;
use App\Customer;
use App\CustomerLog;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request)
    {
        $customer = Customer::query();
        
        $filter = [];
        if(isset($request->filter)) {
            $filter = $request->filter;
            foreach ($filter as $key => $value) {
                if(!empty($value)) {
                    $customer = $customer->where($key, 'like', '%'.$value.'%');
                }
            }
        }
        
        $customer = $customer->paginate(50);
        return view('customers.index',compact('customer','filter'));
    }
    
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        //
    }
    
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        //
    }
    
    /**
    * Display the specified resource.
    *
    * @param  \App\Customer  $customer
    * @return \Illuminate\Http\Response
    */
    public function show(Customer $customer)
    {
        $order = Order::query();
        $order = $order->where('customer_id', '=', $customer->id);
        $order = $order->paginate(10, ['*'], 'order');
        $lastorder = Order::where('customer_id','=',$customer->id)->where('status','>',1)->orderBy('created_at','desc')->first();
        $view=0;
        if($lastorder!=null){
            $lastorder = $lastorder->created_at;
            $view = CustomerLog::where('customer_id','=',$customer->id)->where('created_at','>',$lastorder)->get();
            $view = count($view);
        }else{
            $view = CustomerLog::where('customer_id','=',$customer->id)->get();
            $view = count($view);
        }
        
        return view('customers.show',compact('customer','order','view'));
    }
    
    /**
    * Show the form for editing the specified resource.
    *
    * @param  \App\Customer  $customer
    * @return \Illuminate\Http\Response
    */
    public function edit(Customer $customer)
    {
        return view('customers.create',compact('customer'));
    }
    
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \App\Customer  $customer
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, Customer $customer)
    {
        if($request->change){
            $customer->status=$request->change;
            $customer->save();
            return redirect('/customers/'.$customer->id);
        }else{
            $customer->fill($request->all());
            if($request->card_image != ""){
                $validatedData = $request->validate([
                    'card_image' => 'mimes:jpeg',
                ]);
                $customer->card_image=substr($request->new_image,23);
            }
            $customer->save();
            $request->session()->flash('toast', 'Customer berhasil diubah!');
            return redirect('/customers');
        }
    }
    
    /**
    * Remove the specified resource from storage.
    *
    * @param  \App\Customer  $customer
    * @return \Illuminate\Http\Response
    */
    public function destroy(Customer $customer)
    {
        //
    }
}
