<?php

namespace App\Http\Controllers;

use App\Currency;
use App\CurrencyLog;
use App\DetailCurrency;
use Illuminate\Http\Request;

class RateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $currencies = Currency::all();
        $currency = array();
        foreach($currencies as $c){
            if(count($c->detail_currency)>0){
                array_push($currency,$c);
            }
        }
        return view('rates.index',compact('currency'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $currency = $request->currency;
        $detail_currency = DetailCurrency::where('currency_id','=',$currency)->get();
        foreach($detail_currency as $key => $dc){
            $cl = new CurrencyLog;
            $cl->detail_currency_id = $dc->id;
            $cl->price = $request->price[$key];
            $cl->stock = $request->stock[$key];
            $cl->save();
        }
        return redirect('/rates/'.$currency);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Currency  $currency
     * @return \Illuminate\Http\Response
     */
    public function show($currency)
    {
        $detail_currency = DetailCurrency::where('currency_id','=',$currency)->get();
        $currency_log = new CurrencyLog;
        return view('rates.show',compact('detail_currency','currency_log'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Currency  $currency
     * @return \Illuminate\Http\Response
     */
    public function edit(Currency $currency)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Currency  $currency
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $currency)
    {
        $detail_currency = DetailCurrency::where('currency_id','=',$currency)->get();
        foreach($detail_currency as $key => $dc){
            foreach($dc->today_currency_log as $cl){
                $cl->price = $request->price[$key];
                $cl->stock = $cl->stock+$request->stock[$key];
                $cl->save();
            }
        }
        return redirect('/rates/'.$currency);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Currency  $currency
     * @return \Illuminate\Http\Response
     */
    public function destroy(Currency $currency)
    {
        //
    }
}
