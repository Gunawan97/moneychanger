<?php

namespace App\Http\Controllers;

use App\Currency;
use App\Order;
use App\Customer;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
        $currencies = Currency::all();
        $customer = Customer::where('status','=','1')->get();
        $customer = count($customer);
        $order = Order::where('status','=','1')->get();
        $order = count($order);
        $currency = array();
        foreach($currencies as $c){
            if(count($c->detail_currency)>0){
                array_push($currency,$c);
            }
        }
        return view('home', compact('customer','order','currency'));
    }
}