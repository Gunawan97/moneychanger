<?php

namespace App\Http\Controllers;

use App\DetailCurrency;
use Illuminate\Http\Request;

class DetailCurrencyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DetailCurrency  $detailCurrency
     * @return \Illuminate\Http\Response
     */
    public function show(DetailCurrency $detailCurrency)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DetailCurrency  $detailCurrency
     * @return \Illuminate\Http\Response
     */
    public function edit(DetailCurrency $detailCurrency)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DetailCurrency  $detailCurrency
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DetailCurrency $detailCurrency)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DetailCurrency  $detailCurrency
     * @return \Illuminate\Http\Response
     */
    public function destroy(DetailCurrency $detailCurrency)
    {
        //
    }
}
