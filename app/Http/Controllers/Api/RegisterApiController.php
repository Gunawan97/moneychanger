<?php

namespace App\Http\Controllers\Api;

use Twilio\Rest\Client;
use Twilio\Exceptions\TwilioException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Customer;
use App\Setting;
use DB;


// NOTES
// 464 -> Status code ketika terjadi kesalahan saat update imei customer
// 465 -> Status code ketika terjadi kesalahan saat melakukan registrasi
// 470 -> Status code ketika No KTP tidak ditemukan tapi no Hp telah digunakan
class RegisterApiController extends Controller
{
    public function sendOTPCode(Request $request){
        //Twilio
        $accountSid = env("TWILLIO_ACCOUNT_SID","AC0e2631ae2a097edbf9595abc8d3ed4e7");
        $authToken = env("TWILLIO_AUTH_TOKEN","ca4c1d3654d17651b226f68150fb600b");
        $twilioNumber = env("TWILLIO_NUMBER","+12012548400");
        $digits = 4;
        $otp_code = str_pad(rand(0, pow(10, $digits)-1), $digits, '0', STR_PAD_LEFT);
        $message = "[" . config("app.name") . "] Your activation code " . $otp_code;

        $toNumber = $request->input('phone');

        $client = new Client($accountSid, $authToken);

        try {
                $client->messages->create(
                $toNumber,[
                    "body" => $message,
                    "from" => $twilioNumber
                    ]
                );
                //echo 'Message sent';
                return response([
                    'otp_code'=>$otp_code
                ],200);
            } catch (TwilioException $e) {
                //echo  $e;
                return response([], 404);
            }
    }

    public function RegisterCustomer(Request $request){
        try{
            $idCustomer=Customer::select(DB::raw('id'))
            ->where('card_id','=',$request->input('card_id'))
            ->first();
            
            if($idCustomer){
                $cekHp=Customer::select('phone')
                ->where('id','=',$idCustomer->id)
                ->first();
                
                if($cekHp->phone==$request->input('phone')){
                    
                    $updateCustomer=Customer::findOrFail($idCustomer->id);
                    if( $updateCustomer->status==3){
                        $Message = 'Customer Suspended!';
                        return response([
                            'message' => $Message
                        ],462);
                    }
                    else{
                        try{
                            $updateCustomer->name=$request->input('name');
                            $updateCustomer->imei=$request->input('imei');
                            $updateCustomer->card_image=$request->input('card_image');
                            $updateCustomer->address=$request->input('address');
                            $updateCustomer->status=1;
                            if($updateCustomer->save()){
                                return response([
                                    'customer'=>$updateCustomer
                                ],200);
                            }
                           
                         }
                         catch(\Exception $e){
                            $errMessage = 'Terjadi kesalahan saat proses update imei customer';
                    
                            return response([
                                'error' => 'Error!',
                                'message' => $errMessage
                            ],464);
                           
                         }
                    }
                }
                else{
                    $Message = 'Delete SP pada Hp';
                    return response([
                        'message' => $Message
                    ],460);
                }
            }
            else{
                $cekHp=Customer::select('phone')
                ->where('phone','=',$request->input('phone'))
                ->first();
                
                if($cekHp!=null){
                    $Message = 'No Hp Telah Digunakan';
                    return response([
                        'message' => $Message
                    ],470);
                }
                else{
                    
                    $createCustomer=new Customer;
                    $createCustomer->name=$request->input('name');
                    $createCustomer->imei=$request->input('imei');
                    $createCustomer->card_id=$request->input('card_id');
                    $createCustomer->phone=$request->input('phone');
                    $createCustomer->card_image=$request->input('card_image');
                    $createCustomer->address=$request->input('address');
                    $createCustomer->max_transaction=Setting::select('value')->where('name','like','max_transaction')->first()->value;
                    $createCustomer->status=1;
                    
                    //$file_data = $request->input('card_image');
                    //$file_name = 'image_.png'; //generating unique file name;
                 
                    // if ($file_data != "") { // storing image in storage/app/public Folder
                    //     //Storage::disk('public')->put($file_name, base64_decode($file_data));
                        
                    //     $path = public_path().'/image/';
                    //     $f->move($path,base64_decode($file_data));
                    // }

                    if($createCustomer->save()){
                        return response([
                            'customer' => $createCustomer
                        ],200);
                    }
                    else{
                        $Message = 'Terjadi Kesalahan Saat Melakukan Registrasi';
                        return response([
                            'message' => $Message
                        ],465);
                    }

                }
                
            }
        
        }catch(\Exception $e){
            return response([
                'error' => $e->getCode(),
                'message' => $e->getMessage()
            ],$e->getCode());
        }
        
    }
}
