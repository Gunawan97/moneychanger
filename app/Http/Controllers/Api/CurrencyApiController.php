<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Currency;
use App\DetailCurrency;
use App\CurrencyLog;
use App\Setting;
use App\Customer;
use DB;

class CurrencyApiController extends Controller
{
    public function getCurrencyList(Request $request)
    { 
        try{
            
            $currencyList=Currency::select()->get();
            $filteredCurrencyList=array();
            
            $closetime = Setting::where('name','=','closetime')->first()->value;
            $closetime = Carbon::createFromFormat("h:i A",$closetime);

            $opentime = Setting::where('name','=','opentime')->first()->value;
            $opentime = Carbon::createFromFormat("h:i A",$opentime);
            
            if(Carbon::now()->between($opentime,$closetime)){
                foreach($currencyList as $currencyItem){
                    $detailCurrencyList=DetailCurrency::where('currency_id','=',$currencyItem->id)->get();
                    foreach($detailCurrencyList as $detailCurrencyItem){
                        $detailCurrencyLogCount=CurrencyLog::select(DB::raw('count(id) as countId'))
                        ->where('detail_currency_id','=',$detailCurrencyItem->id)
                        ->where('created_at', '>=', DB::raw('CURDATE()'))
                        ->where('stock','>','0')
                        ->first()->countId;

                        if($detailCurrencyLogCount>0){
                            array_push($filteredCurrencyList,$currencyItem);
                            break;
                        }
                    }
                    
                }
                return response([
                    'currencyList' =>$filteredCurrencyList
                ],200);
            }
            else{
                return response([
                    'currencyList' =>$filteredCurrencyList
                ],200);
            }

        }catch(\Exception $e){
            return response([
                'error' => $e->getCode(),
                'message' => $e->getMessage()
            ],$e->getCode());
        }
        
    }

    public function getDetailCurrencyList($customer_id)
    { 
        try{
            $closetime = Setting::where('name','=','closetime')->first()->value;
            $closetime = Carbon::createFromFormat("h:i A",$closetime);

            $opentime = Setting::where('name','=','opentime')->first()->value;
            $opentime = Carbon::createFromFormat("h:i A",$opentime);

            $idDetailCurrencyList=DetailCurrency::select('id')->get();
            $detailCurrencyList=array();

            $currencyList=Currency::select()->get();
            $filteredCurrencyList=array();
            $maximumOrder=0;

            if(Carbon::now()->between($opentime,$closetime)){
                foreach( $idDetailCurrencyList as $idDetailCurrency){
                    $currencyLogs=CurrencyLog::select('currency_logs.id',
                    'currency_logs.detail_currency_id',
                    'detail_currencies.currency_id',
                    'detail_currencies.nominal',
                    'currency_logs.stock',
                    'currency_logs.price'
                    )
                    ->leftJoin('detail_currencies','detail_currencies.id','=','currency_logs.detail_currency_id')
                    ->where('currency_logs.detail_currency_id','=',$idDetailCurrency->id)
                    ->where('currency_logs.created_at', '>=', DB::raw('CURDATE()'))
                    ->where('currency_logs.stock','>','0')
                    ->first();
                    
                    if($currencyLogs!=null){
                        array_push($detailCurrencyList,$currencyLogs);
                    }
                }
    
               foreach($currencyList as $currencyItem){
                    if($currencyItem->total_stock>0){
                        array_push($filteredCurrencyList,$currencyItem);
                    }
                }
                
                $maximumOrder=Customer::find($customer_id)->max_transaction;
                
                return response([
                    'currencyList'=> $filteredCurrencyList,
                    'detailCurrencyList' =>$detailCurrencyList,
                    'maximumOrder'=> $maximumOrder
                ],200);
            }
            else{
                return response([
                    'currencyList'=> $currencyList,
                    'detailCurrencyList' =>$detailCurrencyList,
                    'maximumOrder'=> $maximumOrder
                ],200);
            }
           
           

        }catch(\Exception $e){
            return response([
                'error' => $e->getCode(),
                'message' => $e->getMessage()
            ],$e->getCode());
        }

    }
    
}
