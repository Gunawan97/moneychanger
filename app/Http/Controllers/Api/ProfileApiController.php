<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Customer;
use App\Courier;
use DB;


// 474 -> Status code saat terjadi image ktp pada hp customer sudah sama dengan image yang terupdate di web dan database
class ProfileApiController extends Controller
{
    public function getCustomerProfileData($customer_id, Request $request){
       
        $customer=Customer::findOrFail($customer_id);
        
        if(md5($customer->card_image)!=$request->input('hash')){
            $message='Image Hp Harus Diupdate !';
            return response([
                'customer'=>[
                    'name'=> $customer->name,
                    'phone'=> $customer->phone,
                    'card_id' =>$customer->card_id,
                    'address' =>$customer->address,
                    'card_image' =>$customer->card_image,
                ],
                'message' => $message
            ],200);
        }else{
            $message='Image Hp Sudah Sama dengan Database !';
            return response([
                'customer'=>[
                    'name'=> $customer->name,
                    'phone'=> $customer->phone,
                    'card_id' =>$customer->card_id,
                    'address' =>$customer->address,
                ],
                'message' => $message
            ],274);
        }
       
            $message='Image Hp Harus Diupdate !';
            return response([
                'customer'=>[
                    'name'=> $customer->name,
                    'phone'=> $customer->phone,
                    'card_id' =>$customer->card_id,
                    'card_image' =>$customer->card_image,
                ],
                'message' => $message
            ],200);
    }
}
