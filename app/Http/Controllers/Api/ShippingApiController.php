<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Order;
use App\DetailOrder;
use App\Customer;
use App\Courier;
use App\Events\CustomerOrder;
use DB;

//NOTES

// 469-> Status code terjadi kesalahan pada saat update Confirmation Shipping

class ShippingApiController extends Controller
{
    public function getShippingList(Request $request)
    { 
        try{
            $shippingList=Order::select(
                'orders.id',
                'orders.order_number',
                'customers.name',
                'customers.address'
                )
                ->leftJoin('customers','customers.id','=','orders.customer_id')
                ->where('orders.courier_id','=',$request->input('courier_id'))
                ->where('orders.status','=',3)
                ->get();
                
                return response([
                    'shippingList' =>$shippingList
                ],200);
                
            }catch(\Exception $e){
                return response([
                    'error' => $e->getCode(),
                    'message' => $e->getMessage()
                ],$e->getCode());
            }
            
        }
        
        public function getDetailShipping($order_id)
        { 
            try{
                
                $shipping=Order::select(
                    'orders.id as order_id',
                    'orders.created_at',
                    'orders.order_number',
                    'customers.name',
                    'customers.address',
                    'orders.total'
                    )
                    ->leftJoin('customers','customers.id','=','orders.customer_id')
                    ->where('orders.id','=',$order_id)
                    ->first();
                    
                    $detailShipping=DetailOrder::select(
                        'detail_orders.id as detail_order_id',
                        'orders.id',
                        'currencies.code',
                        'detail_currencies.nominal',
                        'detail_orders.amount',
                        'detail_orders.price',
                        'detail_orders.total'
                        )
                        ->leftJoin('orders','orders.id','=','detail_orders.order_id')
                        ->leftJoin('customers','customers.id','=','orders.customer_id')
                        ->leftJoin('currency_logs','detail_orders.currency_log_id','=','currency_logs.id')
                        ->leftJoin('detail_currencies','detail_currencies.id','=','currency_logs.detail_currency_id')
                        ->leftJoin('currencies','currencies.id','=','detail_currencies.currency_id')
                        ->where('orders.id','=',$order_id)
                        ->get();
                        
                        $detail_order = DetailOrder::where('order_id','=',$order_id)->get();
                        $total = 0;
                        foreach($detail_order as $d){
                            $total += $d->amount*$d->currency_log->detail_currency->nominal;
                        }
                        
                        return response([
                            'shipping' =>[
                                'id'=>$shipping->order_id,
                                'order_number'=>$shipping->order_number,
                                'name'=>$shipping->name,
                                'address'=>$shipping->address,
                                'total'=>$shipping->total,
                                'delivery_name'=>$shipping->delivery_name,
                                'status_name'=>$shipping->status_name,
                                'created_at'=>$shipping->created_at,
                                'date'=>$shipping->date,
                                'total_nominal'=>$detail_order[0]->currency_log->detail_currency->currency->code." ".$total
                            ],
                            'detailShipping'=> $detailShipping
                        ],200);
                        
                    }catch(\Exception $e){
                        return response([
                            'error' => $e->getCode(),
                            'message' => $e->getMessage()
                        ],$e->getCode());
                    }
                    
                }
                
                public function processUpdateConfirmationShipping($order_id,Request $request){
                    try{
                        
                        $updateOrder=Order::findOrFail($order_id);
                        
                        $updateOrder->receiver_name=$request->input('receiver_name');
                        $updateOrder->image_receipt=$request->input('image_receipt');
                        $updateOrder->latitude=$request->input('latitude');
                        $updateOrder->longitude=$request->input('longitude');
                        $updateOrder->status=4;
                        
                        $kurirName=Courier::findOrFail($updateOrder->courier_id)->username;
                        $updateOrder->save_history($kurirName,4);
                        
                        if($updateOrder->save()){
                            event(new CustomerOrder());
                            return response([
                                
                            ],200);
                        }
                        else{
                            $Message = 'Terjadi Kesalahan pada saat Update Konfirmasi Order';
                            return response([
                                'message' => $Message
                            ],469);
                        }
                        
                        
                    }catch(\Exception $e){
                        return response([
                            'error' => $e->getCode(),
                            'message' => $e->getMessage()
                        ],$e->getCode());
                    }
                    
                }
                
            }
            