<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Order;
use App\CurrencyLog;
use App\DetailOrder;
use App\Customer;
use App\Setting;
use App\Events\CustomerOrder;
use DB;

// NOTES

//STATUS CODE:
//466 -> Stok barang di database <=0
//467 -> Total yang dikirimkan tidak sama dengan calculate total pada database
//468 -> Terjadi Kesalahan Pada Saat Insert Detail Order dan Update CurrencyLog
//674 -> Terdapat Order yang masih Pending
class OrderApiController extends Controller
{

    //Request:customer_id, status
    //return: ListHistoryOrder
    public function getOrderHistoryList(Request $request)
    {  
         try{
            
            if($request->input('status')==0){
                $listHistoryOrder=Order::where('customer_id','=',$request->input('customer_id'))
                ->where('status','!=','4')
                ->where('status','!=','5')     
                ->get();

            }
            if($request->input('status')==1){
                $listHistoryOrder=Order::where('customer_id','=',$request->input('customer_id'))
                ->where('status','!=','1')
                ->where('status','!=','2')  
                ->where('status','!=','3')      
                ->get();
            }
            
            return response([
                'listHistoryOrder' => $listHistoryOrder
            ],200);
        }catch(\Exception $e){
            return response([
                'error' => $e->getCode(),
                'message' => $e->getMessage()
            ],$e->getCode());
        }
       
    }

    public function getDetailHistory($order_id){
        try{
            
            $order=Order::where('orders.id','=',$order_id)
            ->first();
   
            $expiredMinutes=Setting::select('value')
            ->where('name','like','expiredtime')->first()->value;

            $remaining_time=$order->created_at->addMinutes($expiredMinutes)->diffForHumans();
           
            $expiredDate=DB::select(DB::raw('select created_at from orders where id='.$order_id));
            $expiredDate=Carbon::parse($expiredDate[0]->created_at);
            $expiredDate = $expiredDate->addMinutes($expiredMinutes);
            $expiredDate = $expiredDate->format('d/m/Y H:i:s');
            
           

            $accountName=Setting::select('value')
            ->where('name','like','account_name')->first()->value;
            
            $accountNumber=Setting::select('value')
            ->where('name','like','account_number')->first()->value;
        
            return response([
                'order' =>$order,
                'expiredDate' =>$expiredDate,
                'accountName' =>$accountName,
                'accountNumber' =>$accountNumber,
                'remainingTime'=>$remaining_time
            ],200);
        
        }catch(\Exception $e){
            return response([
                'error' => $e->getCode(),
                'message' => $e->getMessage()
            ],$e->getCode());
        }
    }
    public function getDetailOrderList($order_id){
        try{
            $order=Order::select(
                'orders.id',
                'customers.name',
                'customers.address',
                'orders.total',
                'orders.delivery',
                'orders.status'
            )
            ->leftJoin('customers','customers.id','=','orders.customer_id')
            ->where('orders.id','=',$order_id)
            ->first();

            $detailOrderList=DetailOrder::select(
                'detail_orders.id',
                'currencies.name',
                'detail_currencies.nominal',
                'currency_logs.price',
                'detail_orders.total'
            )
            ->leftJoin('orders','orders.id','=','detail_orders.order_id')
            ->leftJoin('customers','customers.id','=','orders.customer_id')
            ->leftJoin('currency_logs','detail_orders.currency_log_id','=','currency_logs.id')
            ->leftJoin('detail_currencies','detail_currencies.id','=','currency_logs.detail_currency_id')
            ->leftJoin('currencies','currencies.id','=','detail_currencies.currency_id')
            ->where('orders.id','=',$order_id)
            ->get();

            return response([
                'order' =>$order,
                'detailOrderList'=> $detailOrderList
            ],200);

        }catch(\Exception $e){
            return response([
                'error' => $e->getCode(),
                'message' => $e->getMessage()
            ],$e->getCode());
        }
    }
    
    public function processCreateOrder(Request $request)
    { 
       

        try{   
                $calculateTotal=0;
                $amount = explode("&", $request->input('amount'));
                $currency_log_id= explode("&", $request->input('currency_log_id'));
                
                $closetime = Setting::where('name','=','closetime')->first()->value;
                $closetime = Carbon::createFromFormat("h:i A",$closetime);

                $opentime = Setting::where('name','=','opentime')->first()->value;
                $opentime = Carbon::createFromFormat("h:i A",$opentime);
                
            if(Carbon::now()->between($opentime,$closetime)){
                
                $countOrderPending=Order::select(DB::raw('count(id) as countId'))
                ->where('customer_id','=',$request->input('customer_id'))
                ->where('status','=','1')
                ->first()->countId;
               
                if($countOrderPending==0){
                    for ($i = 0; $i < count($currency_log_id); $i++) {
                    
                        $currencyLog=CurrencyLog::findOrFail($currency_log_id[$i]);
                        $price=$currencyLog->price;
                        $sisaStok=$currencyLog->stock-$amount[$i];
    
                        if($sisaStok<0){
                            $Message = 'Pembelian Tidak Dapat Diproses Dikarenakan Stok Barang Tidak Mencukupi';
                            return response([
                                'message' => $Message
                            ],466); 
                        }
                       
                        $calculateTotal+=$price*$amount[$i]*$currencyLog->detail_currency->nominal;
                    }
                    if($calculateTotal==$request->input('total')){
                        $calculateUniqTotal=$request->input('total');
    
                       
                        while(true){
                            
                            $countUniqTotal=Order::select(DB::raw('count(id) as countId'))
                            ->where('created_at','>=',DB::raw('CURDATE()'))
                            ->where('total','=',$calculateUniqTotal)
                            ->first()->countId;
                            
                            if($countUniqTotal==0){
                                break;
                            }
                            $calculateUniqTotal+=1;   
                        }
                       
    
                        $createOrder=new Order;
                        $createOrder->customer_id=$request->input('customer_id');
                        $createOrder->total=$calculateUniqTotal;
                        $createOrder->status=1;
                        $createOrder->delivery=$request->input('delivery');
                       
                        $orderToday=Order::select(DB::raw('count(id) as countId'))
                        ->where('created_at','>=',DB::raw('CURDATE()'))->first()->countId;
                        $orderToday=str_pad($orderToday,4,"0",STR_PAD_LEFT);
                        $timeStamp = date( "mdY", strtotime(Carbon::now()));
                        $orderNumber=$timeStamp.$orderToday;
    
                        $createOrder->order_number=$orderNumber;
    
                        $CekInsertDetailOrder=true;
                        

                        $detailOrderList=array();
                        if($createOrder->save()){
                           
                            for ($i = 0; $i < count($currency_log_id); $i++) {
                                $createDetailOrder=new DetailOrder;
                                $createDetailOrder->order_id= $createOrder->id;
                                $createDetailOrder->currency_log_id=$currency_log_id[$i];
                                $createDetailOrder->amount=$amount[$i];
    
                                $currencyLog=CurrencyLog::findOrFail($currency_log_id[$i]);
                                $createDetailOrder->price=$currencyLog->price;
                                $createDetailOrder->total=$currencyLog->price*$amount[$i];
                                
                                $currencyLog->stock-=$amount[$i];
                               
                                if($createDetailOrder->save() && $currencyLog->save()){
                                    array_push($detailOrderList,$createDetailOrder);
                                    $CekInsertDetailOrder=true;
                                }
                                else{
                                    $deleteOrder=Order::where('id',$createOrder->id)->delete();
                                    $deleteDetailOrder=DetailOrder::where('order_id',$createOrder->id)->delete();
                                    $CekInsertDetailOrder=false;
                                    break;
                                }
                                
                            }
                            
                            if($CekInsertDetailOrder==true){
                                
                                $Message = 'Order dan Detail Order Telah Berhasil Ditambahkan';
                                
                                $accountName=Setting::select('value')
                                ->where('name','like','account_name')->first()->value;
                                $accountNumber=Setting::select('value')
                                ->where('name','like','account_number')->first()->value;
                                
                                event(new CustomerOrder());
                                return response([
                                    'message' => $Message,
                                    'order' =>[
                                        'id'=>$createOrder->id,
                                        'order_number'=>$createOrder->order_number,
                                        
                                        ]
                                    // 'detailOrderList'=> $detailOrderList,
                                    // 'expiredDate' =>$expiredDate,
                                    // 'accountName' =>$accountName,
                                    // 'accountNumber' =>$accountNumber
                                ],200);
                            }
                            else{
                                $Message = 'Terjadi Kesalahan Pada Saat Insert Detail Order dan Update CurrencyLog';
                           
                                return response([
                                    'message' => $Message
                                ],468);
                            }
                            
                        }
                    }
                    else{
                        $Message = 'Pembelian Tidak Dapat Diproses dikarenakan Total tidak sesuai dengan Calculate Total Pada Database';
                        return response([
                            'message' => $Message
                        ],467);
                    }

                }
                else{
                    $Message = 'Pembelian Tidak Dapat Diproses dikarenakan Terdapat Order Yang Masih Pending';
                    return response([
                        'message' => $Message
                    ],474);
                }
                
                
            }
            else{
                $Message = 'Pembelian Tidak Dapat Diproses Karena Toko Tutup';
                       
                return response([
                    'message' => $Message
                ],472);
            }   

        }catch(\Exception $e){
            return response([
                'error' => $e->getCode(),
                'message' => $e->getMessage()
            ],$e->getCode());
        }
       
    }

}   
