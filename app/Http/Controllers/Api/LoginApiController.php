<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Customer;
use App\Courier;
use App\CustomerLog;
use DB;

// NOTES

// STATUS CODE:

// 200 -> Status code apabila customer berhasil login
// 460 -> Status code untuk menghapus SP 
// 461 -> Status code saat status customer belum di Approve
// 200 -> Status code saat status customer saat sudah di Approve
// 462 -> Status code saat status customer Suspend
// 463 -> Data Login Kurir sudah dipergunakan pengguna lain
// 471 -> Status kurir suspended
// 473 -> Data kurir tidak ditemukan

class LoginApiController extends Controller
{
    public function getCourierLoginData(Request $request){
        
        try{
            
            $CourierData=Courier::where('username','like',$request->input('username'))
            ->first();
            
            //username ditemukan
            if($CourierData != null){
                //imei sama
                if($CourierData->imei == $request->input('imei')){
                    //Status Approved
                    if($CourierData->status==1){
                        return response([
                            'courierData' =>$CourierData
                        ],200);
                    }else{
                        return response([
                            'courierData' =>$CourierData
                        ],471);
                    }
                }

                //imei beda
                else{
                    return response([
                            
                        ],463);
                }
               
            }

            else{
                //username tidak ditemukan
                $CourierData=Courier::where('imei','like',$request->input('imei'))
                ->first();
                
                //imei tidak ditemukan
                if($CourierData==null){
                    $createKurir=new Courier;
                    $createKurir->imei=$request->input('imei');
                    $createKurir->username=$request->input('username');
                    $createKurir->status=2;
                    if($createKurir->save()){
                        return response([
                            'courierData' =>$createKurir
                        ],471);
                    }
                }

                //imei ditemukan
                else{
                    return response([
                    
                    ],463);
                }
                
            }
            
        }catch(\Exception $e){
            return response([
                'error' => $e->getCode(),
                'message' => $e->getMessage()
            ],$e->getCode());
        }
    }

    public function cekStatus($username){
        $cekKurirData=Courier::
        where('username','like',$username)
        ->first();
       
        if($cekKurirData){
            if($cekKurirData->status==1){
                $message='Status Kurir Active';
                return response([
                   'message' => $message     
                ],200);
            }
            if($cekKurirData->status==2){
                $message='Status Kurir Suspended';
                return response([
                    'message' => $message  
                ],471);
            }
        }
        
        else{
            $message='Data Kurir Tidak Ditemukan';
                return response([
                    'message' => $message  
                ],473);
        }
        

    }
    public function cekApproval($customer_id){
        try{
            $Approval=Customer::select('status')
            ->where('id','=',$customer_id)
            ->first();
            
            if($Approval->status==1){
                
                $Message = 'Customer Belum di Approve!';
                    return response([
                        'message' => $Message
                    ],461);
            }
            if($Approval->status==2){
                $Message = 'Customer Telah sudah di Approve!';
                    return response([
                        'message' => $Message
                    ],200);
            }
            if($Approval->status==3){
                $Message = 'Customer Suspended!';
                    return response([
                        'message' => $Message
                    ],462);
            }
            
        }catch(\Exception $e){
            return response([
                'error' => $e->getCode(),
                'message' => $e->getMessage()
            ],$e->getCode());
        }

    }
    public function getCustomerLoginData(Request $request){
        try{

            $CekCustomerData=Customer::select(DB::raw('count(id) as countId'))
            ->where('imei','=',$request->input('imei'))
            ->where('id','=',$request->input('customer_id'))
            ->first();


                
            if($CekCustomerData->countId>0){
                
                $Approval=Customer::select('status')
                ->where('id','=',$request->input('customer_id'))
                ->first();
                
                if($Approval->status==3){
                    $Message = 'Customer Suspended!';
                        return response([
                            'message' => $Message
                        ],462);
                }

                // $createCustomerLogs= new CustomerLog;
                // $createCustomerLogs->customer_id=$request->input('customer_id');
                // $createCustomerLogs->save();
              
                DB::table('customer_logs')->insert(
                    [
                        'customer_id' => $request->input('customer_id'),
                        'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    ]
                );
                
                $Message = 'Customer Berhasil Login!';
                return response([
                    'message' => $Message
                ],200);
            }

            else{
                $Message = 'Delete SP pada Hp';
                    return response([
                        'message' => $Message
                ],460);
            }
            
        }catch(\Exception $e){
            return response([
                'error' => $e->getCode(),
                'message' => $e->getMessage()
            ],$e->getCode());
        }
    }
}

