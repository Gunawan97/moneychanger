<?php

namespace App\Http\Middleware;

use Illuminate\Http\Request;

use Closure;

class CheckAuthToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $authToken=$request->header('Auth-Token');
        if(!$authToken || $authToken!=env('AUTH_TOKEN', 'Wg6RdO04TD0DDoeb')){
            return response([],480);
        }
        return $next($request);
    }
}
