<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    public function detail_currency(){
        return $this->hasMany('App\DetailCurrency','currency_id');
    }
    
    public function getRangeAttribute(){
        $today=Carbon::now()->startOfDay()->format('Y-m-d');
        $min=0;
        $max=0;
        foreach($this->detail_currency as $key => $dc){
            foreach($dc->currency_log as $cl){
                $date=new Carbon($cl->created_at);
                $date=$date->format('Y-m-d');
                if($date==$today){
                    if($key==0){
                        $min=$cl->price;
                        $max=$cl->price;
                    }else{
                        if($cl->price>$max){
                            $max=$cl->price;
                        }
                        if($cl->price<$min){
                            $min=$cl->price;
                        }
                    }
                }
            }
        }
        if($min==$max){
            return "IDR ".$min."";
        }else{
            return "IDR ".$min." - ".$max;
        }
    }

    public function getTotalStockAttribute(){
        $today=Carbon::now()->startOfDay()->format('Y-m-d');
        $total=0;
        foreach($this->detail_currency as $key => $dc){
            foreach($dc->currency_log as $cl){
                $date=new Carbon($cl->created_at);
                $date=$date->format('Y-m-d');
                if($date==$today){
                    $total+=$dc->nominal*$cl->stock;
                }
            }
        }
        return $total;
    }

    protected $appends =['range','total_stock'];
    protected $guarded =[];
}
