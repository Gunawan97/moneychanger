<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Courier extends Model
{
    use SoftDeletes;
    public function getStatusNameAttribute(){
        if($this->status==1){
            return "Approved";
        }
        else if($this->status==2){
            return "Suspended";
        }
    }

    public function order(){
        return $this->hasMany('App\Order','courier_id');
    }

    protected $appends = ['status_name'];
    protected $guarded = [];
    protected $dates = ['deleted_at'];
}
