<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    public function getStatusNameAttribute(){
        if($this->status==1){
            return "Unapproved";
        }
        else if($this->status==2){
            return "Approved";
        }
        else if($this->status==3){
            return "Suspended";
        }
    }

    public function customer_log(){
        return $this->hasMany('App\CustomerLog','customer_id');
    }

    protected $appends = ['status_name'];
    protected $guarded = ['card_image','new_image'];
}
