<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CurrencyLog extends Model
{
    public function detail_currency(){
        return $this->belongsTo('App\DetailCurrency','detail_currency_id');
    }
    
}
