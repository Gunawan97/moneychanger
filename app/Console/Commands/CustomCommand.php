<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Setting;
use Carbon\Carbon;
use App\Order;

class CustomCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'custom:command';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Expired Order and check close hour';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    public function handle()
    {

        $closetime = Setting::where('name','=','closetime')->first()->value;
        $closetime = Carbon::createFromFormat("h:i A",$closetime);
        $now = Carbon::now();

        if($now->eq($closetime)){
            $order = new Order;
            $order = $order->deleteOrder();
        }
        else if ($now->lt($closetime)){
            $order = new Order;
            $order = $order->deleteExpiredOrder();
        }
    }
}
