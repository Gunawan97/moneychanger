<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailOrder extends Model
{
	public function currency_log(){
		return $this->belongsTo('App\CurrencyLog','currency_log_id');
	}
	public function getTotalNameAttribute(){
		return "IDR " . number_format(floatval($this->total), 0, ",", ".");
	}
	public function getPriceNameAttribute(){
		return "IDR " . number_format(floatval($this->price), 0, ",", ".");
	}
	protected $guarded = ['total_name','price_name'];
}
