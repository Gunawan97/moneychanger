<?php

namespace App;
use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;

class HistoryChange extends Model
{
    public function getDateAttribute(){
        return Carbon::parse($this->created_at)->format('d/m/Y H:i:s');
    }
    protected $appends = ['date'];
}
