<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class DetailCurrency extends Model
{
    public function currency(){
        return $this->belongsTo('App\Currency','currency_id');
    }

    public function currency_log(){
        return $this->hasMany('App\CurrencyLog','detail_currency_id');
    }

    public function today_currency_log(){
        $today=Carbon::now()->startOfDay();
        $tomorrow=Carbon::tomorrow()->startOfDay();
        return $this->hasMany('App\CurrencyLog','detail_currency_id')->whereDate('created_at','>=',$today)->whereDate('created_at','<=',$tomorrow);
    }
public function getNominalNameAttribute(){
    return number_format(floatval($this->nominal), 0, ",", ".");
}
    protected $guarded =['nominal_name'];
}
