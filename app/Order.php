<?php

namespace App;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public function customer(){
        return $this->belongsTo('App\Customer','customer_id');
    }
    public function courier(){
        return $this->belongsTo('App\Courier','courier_id')->withTrashed();
    }
    public function detail_order(){
        return $this->hasMany('App\DetailOrder','order_id');
    }
    public function history_change(){
        return $this->hasMany('App\HistoryChange','order_id')->orderBy('id','desc');
    }
    public function restore(){
        foreach($this->detail_order as $d){
            //return  $d->currency_log->stock." ".$d; 
           $d->currency_log->stock += $d->amount;
           $d->currency_log->save();
       }
   }

   public function deleteExpiredOrder(){
    $now = Carbon::now();
    $expiredtime = Setting::where('name','=','expiredtime')->first()->value;
    $now->addMinutes(-$expiredtime);
    $now = $now->format('Y-m-d H:i:s');
    $order = Order::all()->where('status','=',1)->where('created_at','<',$now);

    foreach($order as $o){
        $o->status=5;
        $o->save();
        $o->save_history('Sistem',5);
    }
}

public function deleteOrder(){
    $order = Order::all()->where('status','=',1);

    foreach($order as $o){
        $o->status=5;
        $o->save();
        $o->save_history('Sistem',5);
    }
}

public function save_history($user, $status){
    if($status == 2){
        $comment = "Order telah diproses";
    }
    else if($status == 3){
        $comment = "Order telah dikirim";
    }
    else if($status == 4){
        $comment = "Order telah diterima";
    }
    else if($status == 5){
        $comment = "Order telah dibatalkan";
    }
    else if($status == 6){
        $comment = "Metode Pengiriman diganti";
    }
    $history_change = new HistoryChange;
    $history_change->order_id = $this->id;
    $history_change->user = $user;
    $history_change->comment = $comment;
    $history_change->save();

}
public function getStatusNameAttribute(){
    if($this->status==1){
        return "Order";
    }
    else if($this->status==2){
        return "Proses";
    }
    else if($this->status==3){
        return "Dikirim";
    }
    else if($this->status==4){
        return "Diterima";
    }
    else if($this->status==5){
        return "Dibatalkan";
    }
}
public function getDeliveryNameAttribute(){
    if($this->delivery==1){
        return "Deliver to address";
    }
    else if($this->delivery==2){
        return "Pick up in store";
    }
}
public function getTotalNameAttribute(){
    return "IDR " . number_format(floatval($this->total), 0, ",", ".");
}
public function getTotalNominalAttribute(){
    $total = 0;
    foreach($this->detail_order as $d){
        $total += $d->amount*$d->currency_log->detail_currency->nominal;
    }
    return $this->detail_order[0]->currency_log->detail_currency->currency->code." ".$total;
}

public function getTotalBuyAttribute(){
    $total = 0;
    $currency = array();
    foreach($this->detail_order as $d){
        $cek = 1;
        foreach($currency as $c){
            if($c == $d->currency_log->detail_currency->currency->code){
                $cek = 2;
                break;
            }
        }
        if($cek == 1){
            array_push($currency, $d->currency_log->detail_currency->currency->code);
        }
    }

    $string = "";
    foreach($currency as $key => $c){
        $total = 0;
        foreach ($this->detail_order as $d) {
            if($c == $d->currency_log->detail_currency->currency->code){
                $total += $d->amount*$d->currency_log->detail_currency->nominal;
            }
        }
        $string = $string." ".$c." ".$total;
        if($key < count($currency)-1){
            $string = $string.", ";
        }
    }
    return $string;
}

public function getDateAttribute(){
    return Carbon::parse($this->created_at)->format('d/m/Y H:i:s');
}
protected $guarded = [];
protected $appends = ['status_name','delivery_name','date','total_nominal','total_name','total_buy'];
}
