  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="nav sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        @if(Auth::user()->role==1)
        <li id="dashboardPage" class="{{active('home')}}">
          <a href="/home">
            <i class="fa fa-dashboard"></i>
            <span>Dashboard</span>
          </a>
        </li>
        <li id="orderPage" class="{{active('orders.*')}}">
          <a href="/orders">
            <i class="fa fa-shopping-cart"></i>
            <span>Orders</span>
          </a>
        </li>
        <li id="currencyPage" class="{{active('currencies.*')}}">
          <a href="/currencies">
            <i class="fa fa-money"></i>
            <span>Currencies</span>
          </a>
        </li>
        <li id="ratePage" class="{{active('rates.*')}}">
          <a href="/rates">
            <i class="fa fa-bank"></i>
            <span>Rates</span>
          </a>
        </li>
        <li id="customerPage" class="{{active('customers.*')}}">
          <a href="/customers">
            <i class="fa fa-users"></i>
            <span>Customers</span>
          </a>
        </li>
        <li id="courierPage" class="{{active('couriers.*')}}">
          <a href="/couriers">
            <i class="fa fa-motorcycle"></i>
            <span>Couriers</span>
          </a>
        </li>
        <li id="reportPage" class="treeview {{active('reports.*')}} {{active('reports_view.*')}} ">
          <a href="#">
            <i class="fa fa-files-o"></i> <span>Laporan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul  class="treeview-menu">
            <li class="{{active('reports.*')}}">
              <a href="/reports" >
                <i class="fa fa-book"></i>
                <span>Penjualan</span>
              </a>
            </li>
            <li class="{{active('reports_view.*')}}">
              <a href="/reports_view">
                <i class="fa fa-file-o"></i>
                <span>Customer Log</span>
              </a>
            </li>
          </ul>
        </li>
        
        <li id="settingPage" class="{{active('settings.*')}}">
          <a href="/settings">
            <i class="fa fa-wrench"></i>
            <span>Settings</span>
          </a>
        </li>
        <li id="userPage" class="{{active('users.*')}}">
          <a href="/users">
            <i class="fa fa-users"></i>
            <span>Users</span>
          </a>
        </li>
        @elseif(Auth::user()->role==2)
        <li id="dashboardPage" class="{{active('home')}}">
          <a href="/home">
            <i class="fa fa-dashboard"></i>
            <span>Dashboard</span>
          </a>
        </li>
        <li id="orderPage" class="{{active('orders.*')}}">
          <a href="/orders">
            <i class="fa fa-shopping-cart"></i>
            <span>Orders</span>
          </a>
        </li>
        <li id="ratePage" class="{{active('rates.*')}}">
          <a href="/rates">
            <i class="fa fa-bank"></i>
            <span>Rates</span>
          </a>
        </li>
        <li id="courierPage" class="{{active('couriers.*')}}">
          <a href="/couriers">
            <i class="fa fa-motorcycle"></i>
            <span>Couriers</span>
          </a>
        </li>
        @endif
        <li id="logout">
          <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
            <i class="fa fa-power-off"></i>
            <span>Logout</span>
          </a>
          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
          </form>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>