<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
  @include("layouts._head")
  
  @stack('styles')
  <style>
  .skin-blue .main-header .logo {
    background-color: rgb(29,79,79)!important;
  }
  .skin-blue .main-header .navbar {
    background-color: rgb(41,115,116)!important;
  }
</style>
<meta name="csrf-token" content="{{ csrf_token() }}">
</head>

<body class="skin-blue" data-spy="scroll" data-target="#scrollspy">
  <div class="wrapper">
    <!-- Main Header -->
    <header class="main-header">
      <!-- Logo -->
      <a href="../index2.html" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>A</b>LT</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Money</b>Changer</span>
      </a>
      <!-- Header Navbar: style can be found in header.less -->
      <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
          <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <li class="dropdown dropdown-notifications">
              <a href="/orders">
                <i data-count="0" class="glyphicon glyphicon-bell notification-icon"></i>
              </a>
            </li>
          </ul>
          <ul class="nav navbar-nav">
            <li class="dropdown user user-menu">
              <a class="dropdown-toggle" data-toggle="dropdown">
                <span><b>{{Auth::user()->name}}</b></span>
              </a>
            </li>
          </ul>
        </div>
      </nav>
    </header>
    
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <div class="sidebar" id="scrollspy"></div>
      <!-- Left side column. contains the logo and sidebar -->
      <section class="nav sidebar-menu">
        <!-- Sidebar user panel (optional) -->
        @include('layouts._sidebar_menu')
      </section>
      <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <div class='row'>
          <div class="col-md-6">
            <h1 class="title-header">
              @yield('title')
            </h1>
          </div>
          <div class="col-md-6 text-right">
            @yield('actionbtn')
          </div>
        </div>
      </section>

      <!-- Main content -->
      <section class="content">
        @if (Session::has('toast'))
        <div class="alert alert-success alert-dismissible">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <h4><i class="icon fa fa-check"></i> Success!</h4>
          {!! session('toast') !!}
        </div>
        @endif
        @if (Session::has('fail'))
        <div class="alert alert-danger alert-dismissible">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <h4><i class="icon fa fa-times"></i> Failed!</h4>
          {!! session('fail') !!}
        </div>
        @endif
        @yield('content')

      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    @include('layouts._footer')
  </div>
  <!-- ./wrapper -->

  @stack('scripts')
  <script type="text/javascript">
    var notificationsWrapper   = $('.dropdown-notifications');
    var notificationsToggle    = notificationsWrapper.find('a');
    var notificationsCountElem = notificationsToggle.find('i[data-count]');
    var notificationsCount     = parseInt(notificationsCountElem.data('count'));

    if (notificationsCount <= 0) {
      notificationsWrapper.hide();
    }

    var pusher = new Pusher('8d040d268e19e9391ae8', {
      cluster: 'ap1',
      encrypted: true
    });
    var channel = pusher.subscribe('customer-order');

    channel.bind('App\\Events\\CustomerOrder', function(data) {
      notificationsCount += 1;
      notificationsCountElem.attr('data-count', notificationsCount);
      notificationsWrapper.show();
    });
  </script>
</body>
</html>