@extends('layouts.app')

@section('title')
Detail Rate
@endsection

@section('content')
<div class="row">
	<section class="col-md-12">
        <div class="box">
            <div class="box-header">
                <div class="row">
                    <div class="col-md-6">
                        <label>Date {{date("d/m/Y")}}</label>
                    </div>
                    <div class="col-md-6">
                        <label>Default Rates</label> <input type="text" name="defrates" id="defrates">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <label>Currency {{$detail_currency[0]->currency->code}} - {{$detail_currency[0]->currency->name}}</label>
                    </div>
                    <div class="col-md-6">
                        <label><button class="btn btn-primary" type="submit" onclick="return fillrates()">Generate Nominal</button></label>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nominal</th>
                                <th>Stok Saat Ini</th>
                                <th>Stok</th>
                                <th>Rate</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($detail_currency[0]->today_currency_log)>0)
                            {!! Form::model($currency_log, ['route' => ['rates.update', $detail_currency[0]->currency->id], 'method'=>'PATCH','enctype'=>'multipart/form-data', 'role' => 'form']) !!}
                            @else
                            {!! Form::model($currency_log, ['route' => ['rates.store'], 'method'=>'POST','enctype'=>'multipart/form-data', 'role' => 'form']) !!}
                            @endif
                            {!!Form::hidden('currency', $detail_currency[0]->currency->id, ['class'=> 'form-control']) !!}
                            @foreach($detail_currency as $key=>$dc)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$dc->nominal}}</td>
                                <td>
                                    @if(count($detail_currency[0]->today_currency_log)>0)
                                    {{$dc->today_currency_log[0]->stock}}
                                    @else
                                    0
                                    @endif
                                </td>
                                <td>
                                    <div class="input-group">
                                        <span class="input-group-addon">+</span>{!!Form::number('stock[]', 0, ['class'=> 'form-control','required' => 'required']) !!}
                                    </div>
                                </td>
                                <td>
                                    <div class="input-group">
                                        <span class="input-group-addon">IDR</span>
                                        @if(count($detail_currency[0]->today_currency_log)>0)
                                        {!!Form::number('price[]', $dc->today_currency_log[0]->price, ['class'=> 'price form-control','required' => 'required']) !!}
                                        @else
                                        {!!Form::number('price[]', 0, ['class'=> 'price form-control','required' => 'required']) !!}
                                        @endif
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <button type="submit" style="margin:8px;" class="btn btn-primary pull-right">Simpan</button>
                </div>
            </div>
        </div>
    </section>
</div>
@stop
<script type="text/javascript">
    function fillrates(){
        var rates=document.getElementById('defrates').value;
        var prices=document.getElementsByClassName('price');
        for (i = 0; i < prices.length; i++) { 
            prices[i].value = rates;
        }
        return false;
    }
</script>
