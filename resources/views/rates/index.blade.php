@extends('layouts.app')

@section('title')
View Rate
@endsection

@section('content')
<div class="box">
	<div class="box-header">
		<div class="row">
			
		</div>
	</div>
	<!-- /.box-header -->
	<div class="box-body">
		<table id="example1" class="table table-bordered table-striped">
			<thead>
				<tr>
					<th>No</th>
					<th>Currency</th>
					<th>Stok</th>
					<th>Rate</th>
					<th>Show Detail</th>
				</tr>
			</thead>
			<tbody>
				@foreach($currency as $c)
				<tr>
					<td>{{$c->id}}</td>
					<td>{{$c->code}} - {{$c->name}}</td>
					<td>{{$c->code}} {{$c->total_stock}}</td>
					<td>{{$c->range}}</td>
					<td>
						<a href="{{route('rates.show',$c->id)}}">
							<button class="btn btn-primary">
								Show
							</button>
						</a>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>
@stop