@extends('layouts.app')
@push('styles')
<style>  
table {  
    font-family: arial, sans-serif;  
    border-collapse: collapse;  
    width: 100%;  
}  

td, th {  
    border: 1px solid #dddddd;  
    text-align: left;  
    padding: 8px;  
}  

tr:nth-child(even) {  
    background-color: #dddddd;  
}  
</style>  
@endpush
@section('title')
View Laporan
@endsection

@section('content')
<div class="row" style="margin-bottom:2vh;">
    <!-- Search User -->
    <form method="get" id="search-form">
        <div class="col-md-6">
            <div class="input-group no-border">
                <input type="text" class="form-control pull-right" name="filter[date]" value="{{ !empty($filter['date']) ? $filter['date'] : '' }}" id="reservation">
                <span class="input-group-addon icon-search"><i class="fa fa-search"></i></span>
            </div>
            
        </div>
        <div class="col-md-3">
            <button type="submit" class="btn btn-primary btn-block">Cari</button>
        </div>
    </form>
</div>
<div class="box">
  <div class="box-header">
    <div class="row">
      
    </div>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
        <input type="button" id="create_pdf" value="Generate PDF">  
        <form class="form" style="max-width: none; width: 1005px;">  


            <center><h2>MONEYCHANGER</h2>  </center>
            <center><h3>Laporan Customer Log</h3></center> 
            <center><h3>{{ !empty($filter['date']) ? $filter['date'] : $now.' - '.$now }}</h3></center> 
            <table id="example1" class="table table-bordered table-striped">
             <thead>
                <tr>
                    <th>No</th> 
                    <th>Tanggal</th>  
                    <th>Customer</th>  
                </tr>
            </thead>
            <tbody>
                @foreach($customer_log as $key => $c)
                <tr>
                   <td>{{$key + 1}}</td>
                   <td>{{$c->order_number}}</td>
                   <td>{{$c->created_at}}</td>
                   <td>{{$c->customer->name}}</td>
               </tr>
               @endforeach
           </tbody>
       </table>  
   </form>  
</div>
</div>
</div>
@stop
@push('scripts')

<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.5/jspdf.min.js"></script>  
<script>  
    (function () {  
        var  
        form = $('.form'),  
        cache_width = form.width(),  
         a4 = [595.28, 841.89]; // for a4 size paper width and height  

         $('#create_pdf').on('click', function () {  
            $('body').scrollTop(0);  

            var tanggal = document.getElementById("reservation").value;
            createPDF(tanggal);  
        });  
        //create pdf  
        function createPDF($tanggal) {  
            getCanvas().then(function (canvas) {  
                var  
                img = canvas.toDataURL("image/png"),  
                doc = new jsPDF({  
                   unit: 'px',  
                   format: 'a4'  
               });  
                doc.addImage(img, 'JPEG', 20, 20);  
                var filename = "Laporan Customer Log "+$tanggal+".pdf";

                doc.save(filename);  
                form.width(cache_width);  
            });  
        }  

        // create canvas object  
        function getCanvas() {  
            form.width((a4[0] * 1.33333) - 80).css('max-width', 'none');  
            return html2canvas(form, {  
                imageTimeout: 2000,  
                removeContainer: true  
            });  
        }  

    }());  
</script>  
<script>  
    /* 
 * jQuery helper plugin for examples and tests 
 */  
 (function ($) {  
    $.fn.html2canvas = function (options) {  
        var date = new Date(),  
        $message = null,  
        timeoutTimer = false,  
        timer = date.getTime();  
        html2canvas.logging = options && options.logging;  
        html2canvas.Preload(this[0], $.extend({  
            complete: function (images) {  
                var queue = html2canvas.Parse(this[0], images, options),  
                $canvas = $(html2canvas.Renderer(queue, options)),  
                finishTime = new Date();  

                $canvas.css({ position: 'absolute', left: 0, top: 0 }).appendTo(document.body);  
                $canvas.siblings().toggle();  

                $(window).click(function () {  
                    if (!$canvas.is(':visible')) {  
                        $canvas.toggle().siblings().toggle();  
                        throwMessage("Canvas Render visible");  
                    } else {  
                        $canvas.siblings().toggle();  
                        $canvas.toggle();  
                        throwMessage("Canvas Render hidden");  
                    }  
                });  
                throwMessage('Screenshot created in ' + ((finishTime.getTime() - timer) / 1000) + " seconds<br />", 4000);  
            }  
        }, options));  

        function throwMessage(msg, duration) {  
            window.clearTimeout(timeoutTimer);  
            timeoutTimer = window.setTimeout(function () {  
                $message.fadeOut(function () {  
                    $message.remove();  
                });  
            }, duration || 2000);  
            if ($message)  
                $message.remove();  
            $message = $('<div ></div>').html(msg).css({  
                margin: 0,  
                padding: 10,  
                background: "#000",  
                opacity: 0.7,  
                position: "fixed",  
                top: 10,  
                right: 10,  
                fontFamily: 'Tahoma',  
                color: '#fff',  
                fontSize: 12,  
                borderRadius: 12,  
                width: 'auto',  
                height: 'auto',  
                textAlign: 'center',  
                textDecoration: 'none'  
            }).hide().fadeIn().appendTo('body');  
        }  
    };  
})(jQuery);  

</script>  
<script>
  $(function () {
    $('#reservation').daterangepicker({locale: {format: 'DD/MM/YYYY'}})
})
</script>
@endpush