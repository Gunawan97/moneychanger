@extends('layouts.app')

@section('title')
Detail Customer
@endsection
@push('styles')
<style>
	#myImg {
		border-radius: 5px;
		cursor: pointer;
		transition: 0.3s;
	}
	
	#myImg:hover {opacity: 0.7;}
	
	/* The Modal (background) */
	.modal {
		display: none; /* Hidden by default */
		position: fixed; /* Stay in place */
		z-index: 1; /* Sit on top */
		padding-top: 100px; /* Location of the box */
		left: 0;
		top: 0;
		width: 100%; /* Full width */
		height: 100%; /* Full height */
		overflow: auto; /* Enable scroll if needed */
		background-color: rgb(0,0,0); /* Fallback color */
		background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
	}
	
	/* Modal Content (image) */
	.modal-content {
		margin: auto;
		display: block;
		width: 80%;
		max-width: 700px;
	}
	
	/* Caption of Modal Image */
	#caption {
		margin: auto;
		display: block;
		width: 80%;
		max-width: 700px;
		text-align: center;
		color: #ccc;
		padding: 10px 0;
		height: 150px;
	}
	
	/* Add Animation */
	.modal-content, #caption {    
		-webkit-animation-name: zoom;
		-webkit-animation-duration: 0.6s;
		animation-name: zoom;
		animation-duration: 0.6s;
	}
	
	@-webkit-keyframes zoom {
		from {-webkit-transform:scale(0)} 
		to {-webkit-transform:scale(1)}
	}
	
	@keyframes zoom {
		from {transform:scale(0)} 
		to {transform:scale(1)}
	}
	
	/* The Close Button */
	.close {
		position: absolute;
		top: 50px;
		right: 50px;
		color: #f1f1f1;
		font-size: 40px;
		font-weight: bold;
		transition: 0.3s;
	}
	
	.close:hover,
	.close:focus {
		color: #bbb;
		text-decoration: none;
		cursor: pointer;
	}
	
	/* 100% Image Width on Smaller Screens */
	@media only screen and (max-width: 700px){
		.modal-content {
			width: 100%;
		}
	}
</style>
@endpush
@section('content')
<div class="row">
	<section class="col-md-12">
		<div class="box">
			<div class="box-header">
				<div class="row">
					<div class="col-md-12" >
						{!! Form::model($customer, ['route' => ['customers.update', $customer->id],'method'=>'PATCH', 'role' => 'form']) !!}  
						@if($customer->status==1)
						<button type="submit"  name="change" value="3" style="margin:8px;" class="btn btn-danger col-md-2 pull-right">Suspend</button>
						<button type="submit" name="change" value="2" style="margin:8px;" class="btn btn-primary col-md-2 pull-right">Approve</button>
						@elseif($customer->status==2)
						<button type="submit"  name="change" value="3" style="margin:8px;" class="btn btn-danger col-md-2 pull-right">Suspend</button>
						@elseif($customer->status==3)
						<button type="submit" name="change" value="2" style="margin:8px;" class="btn btn-primary col-md-2 pull-right">Approve</button>
						@endif
						{!! Form::close() !!}
						<a href="{{route('customers.edit',$customer->id)}}">
							<button class="btn btn-warning col-md-2 pull-right" style="margin:8px;">
								Edit
							</button>
						</a>
					</div>
				</div>
			</div>
			<div class="box-body">
				<div class="row">
					<div class="col-md-6">
						<div class="col-md-4">
							<label>Nama</label>
							<p>{{$customer->name}}</p>
						</div>
						<div class="col-md-4">
							<label>Nomor KTP</label>
							<p>{{$customer->card_id}}</p>
						</div>
						<div class="col-md-4">
							<label>Alamat</label>
							<p>{{$customer->address}}</p>
						</div>
						<div class="col-md-4">
							<label>Nomor Telpon</label>
							<p>{{$customer->phone}}</p>
						</div>
						<div class="col-md-4">
							<label>IMEI</label>
							<p>{{$customer->imei}}</p>
						</div>
						<div class="col-md-4">
							<label>Status</label>
							<p>{{$customer->status_name}}</p>
						</div>
					</div>
					<div class="col-md-2">
						<div class="row">
							<label>Views After Last Order</label>
							<p>{{$view}}</p>
						</div>
						<div class="row">
							<label>Maksimum Transaksi</label>
							<p>{{$customer->max_transaction}}</p>
						</div>
					</div>
					<div class="col-md-4">
						<div class="row">
							<label>Foto KTP</label>
						</div>
						<div class="row">
							<img src="data:image/jpg;base64,{{$customer->card_image}}" height="200px" id="myImg" alt="KTP">
						</div>
					</div>
				</div>
			</div>
		</section>
		<div id="myModal" class="modal">
			<span class="close">&times;</span>
			<img class="modal-content" id="img01">
			<div id="caption"></div>
		</div>
		<section class="col-md-12">
			<div class="box">
				<div class="box-header">
					<div class="row">
						<div class="col-md-3">
							History Order
						</div>
					</div>
				</div>
				
				<!-- /.box-header -->
				<div class="box-body" style="height: 65vh;overflow-y:visible">
					<div class="row">
						<div class="col-md-12" style="height: 400px;">
							<table id="example1" class="table table-bordered table-striped " >
								<thead>
									<tr>
										<th>No</th>
										<th>Mata Uang</th>
										<th>Total Order</th>
										<th>Total IDR</th>
										<th>Tanggal</th>
										<th>Status</th>
										<th>Show</th>
									</tr>
								</thead>
								<tbody>
									@foreach($order as $key => $a)
									<tr> 
										<td>{{ $key+1}}</td>
										<td>{{ $a->detail_order[0]->currency_log->detail_currency->currency->code }} - {{ $a->detail_order[0]->currency_log->detail_currency->currency->name }}</td>
										<td>{{ $a->total_nominal }}</td>
										<td>{{ $a->total }}</td>
										<td>{{ $a->created_at }}</td>
										<td>{{ $a->status_name }}</td>
										<td>
											<a href="{{route('orders.show',$a->id)}}" class="btn btn-primary">
												Show Order
											</a>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
							
							{{ $order->links() }}
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
	<script>
		// Get the modal
		var modal = document.getElementById('myModal');
		
		// Get the image and insert it inside the modal - use its "alt" text as a caption
		var img = document.getElementById('myImg');
		var modalImg = document.getElementById("img01");
		var captionText = document.getElementById("caption");
		img.onclick = function(){
			modal.style.display = "block";
			modalImg.src = this.src;
			captionText.innerHTML = this.alt;
		}
		
		// Get the <span> element that closes the modal
		var span = document.getElementsByClassName("close")[0];
		// When the user clicks on <span> (x), close the modal
		span.onclick = function() { 
			modal.style.display = "none";
		}
		</script>
	@stop