@extends('layouts.app')

@section('title')
View Customer
@endsection

@section('content')

<div class="row" style="margin-bottom:2vh;">
	<!-- Search User -->
	<form method="get" id="search-form">
		<div class="col-md-2">
			<div class="input-group no-border">
				<input type="text" class="form-control column-filter" name="filter[name]" placeholder="Nama" value="{{ !empty($filter['name']) ? $filter['name'] : '' }}"/>
				<span class="input-group-addon icon-search"><i class="fa fa-search"></i></span>
			</div>
		</div>
		<div class="col-md-3">
			<div class="input-group no-border">
				<input type="text" class="form-control column-filter" name="filter[address]" placeholder="Alamat" value="{{ !empty($filter['address']) ? $filter['address'] : '' }}"/>
				<span class="input-group-addon icon-search"><i class="fa fa-search"></i></span>
			</div>
		</div>
		<div class="col-md-3">
			<div class="input-group no-border">
				<input type="text" class="form-control column-filter" name="filter[card_id]" placeholder="No. Identitas" value="{{ !empty($filter['card_id']) ? $filter['card_id'] : '' }}"/>
				<span class="input-group-addon icon-search"><i class="fa fa-search"></i></span>
			</div>
		</div>
		<div class="col-md-2">
			<div class="input-group no-border">
				<select name="filter[status]" class="form-control column-filter">
					@if(!empty($filter['status']))
					<option value="0" {{ ($filter['status'])==0 ? 'selected' : '' }}>Semua</option>
					<option value="1" {{ ($filter['status'])==1 ? 'selected' : '' }}>Unapproved</option>
					<option value="2" {{ ($filter['status'])==2 ? 'selected' : '' }}>Approved</option>
					<option value="3" {{ ($filter['status'])==3 ? 'selected' : '' }}>Suspended</option>
					@else
					<option value="0" >Semua</option>
					<option value="1" >Unapproved</option>
					<option value="2" >Approved</option>
					<option value="3" >Suspend</option>
					@endif
				</select>
				<span class="input-group-addon icon-search"><i class="fa fa-search"></i></span>
			</div>
		</div>
		<div class="col-md-2">
			<button type="submit" class="btn btn-primary btn-block">Cari</button>
		</div>
	</form>
</div>
<div class="box">
	<div class="box-header">
		<div class="row">
			
		</div>
	</div>
	<!-- /.box-header -->
	<div class="box-body">
		<table id="example1" class="table table-bordered table-striped">
			<thead>
				<tr>
					<th>ID</th>
					<th>Nama</th>
					<th>Nomor KTP</th>
					<th>Alamat</th>
					<th>Telpon</th>
					<th>Status</th>
					<th>Show Detail</th>
				</tr>
			</thead>
			<tbody>
				@foreach($customer as $c)
				<tr>
					<td>{{$c->id}}</td>
					<td>{{$c->name}}</td>
					<td>{{$c->card_id}}</td>
					<td>{{$c->address}}</td>
					<td>{{$c->phone}}</td>
					<td style="vertical-align:middle;">
						<center>
						@if($c->status==1)
						<span class="label label-warning" style="font-size:12px;font-weight:400;">
						@elseif($c->status==2)
						<span class="label label-success" style="font-size:12px;font-weight:400;">
						@elseif($c->status==3)
						<span class="label label-danger" style="font-size:12px;font-weight:400;">
						@endif
						{{$c->status_name}}
						</span>
						</center>
					</td>
					<td>
						<a href="{{route('customers.show',$c->id)}}">
							<button class="btn btn-primary">
								Show
							</button>
						</a>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
		@php
		$filters = [];
		foreach ($filter as $key => $value) {
		$filters['filter['. $key .']'] = $value;
	}
	@endphp
	{{ $customer->appends($filters)->links() }}
</div>
</div>
@stop