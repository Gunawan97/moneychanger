@extends('layouts.app')

@section('title')
Ubah Customer
@endsection
@section('content')

<section class="form-candidate">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::model($customer, ['route' => ['customers.update', $customer->id], 'method'=>'PATCH','enctype'=>'multipart/form-data', 'role' => 'form']) !!}
        <div class="box-body">
          <div class="row">
            <div class="form-group col-md-12">
              <label>Nama</label>
              {!! Form::text('name', $customer->name, ['class'=> 'form-control','required' => 'required']) !!}
            </div>
            <div class="form-group col-md-12">
              <label>Nomor KTP</label>
              {!! Form::number('card_id', $customer->card_id, ['class'=> 'form-control','required' => 'required']) !!}
            </div>
            <div class="form-group col-md-12">
              <label>Alamat</label>
              {!! Form::text('address', $customer->address, ['class'=> 'form-control','required' => 'required']) !!}
            </div>
            <div class="form-group col-md-12">
              <label>Telpon</label>
              {!! Form::text('phone', $customer->phone, ['class'=> 'form-control','required' => 'required']) !!}
            </div>
            <div class="form-group col-md-12">
              <label>IMEI</label>
              {!! Form::text('imei', $customer->imei, ['class'=> 'form-control','required' => 'required']) !!}
            </div>
            <div class="form-group col-md-12">
              <label>Maksimum Transaksi</label>
              {!! Form::number('max_transaction', $customer->max_transaction, ['class'=> 'form-control','required' => 'required']) !!}
            </div>
            <div class="form-group col-md-12">
              <label>Foto KTP</label>
              <div class="form-group col-md-12">
                <img src="data:image/jpg;base64,{{$customer->card_image}}" height="200px" alt="KTP">
              </div>
            </div>
            <div class="form-group col-md-12">
              <label>Ganti Foto KTP</label>
              {!! Form::file('card_image', ['id' => 'images','class'=> 'form-control', 'onchange' => 'preview_images()', 'accept' => '.jpg,.jpeg']) !!}
              @if ($errors->any())
              <div class="alert alert-danger">
                <ul>
                  @foreach ($errors->all() as $error)
                      {{ $error }}
                  @endforeach
                </ul>
              </div>
              @endif
            </div>
            <div class="form-group col-md-12">
              <div class="row" id="image_preview"></div>
              {!! Form::hidden('new_image', null, ['id' => 'new_image', 'class'=> 'form-control']) !!}
            </div>
          </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          <div class="col-md-12">
            <button type="submit" class="btn btn-primary">Simpan</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
<script>
  function preview_images() 
  {
    var reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]);
    var gambar;
    reader.onload = function () {
      gambar = reader.result;
      document.getElementById('image_preview').innerHTML = "";
      $('#image_preview').append("<div class='form-group col-md-12'><img src='"+gambar+"' height='200px'></div>");
      document.getElementById('new_image').value = gambar;
    };
  }
</script>
</section>
@endsection