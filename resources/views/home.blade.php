@extends('layouts.app')
@section('title')
Dashboard
@endsection

@section('content')

<div class="col-md-6">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Nilai Tukar Hari Ini</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered">
                <tr>
                    <th style="width: 10px">#</th>
                    <th>Mata Uang</th>
                    <th>Rate</th>
                    <th style="width: 40px">Show</th>
                </tr>
                @foreach($currency as $c)
                <tr>
                    <td>{{$c->id}}</td>
                    <td>{{$c->name}}</td>
                    <td>{{$c->range}}</td>
                    <td>
                        <a href="{{route('rates.show',$c->id)}}">
                            <button class="btn btn-warning">
                                Show
                            </button>
                        </a>
                    </td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>
</div>
<div class="col-lg-3 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-aqua">
        <div class="inner">
            <h3>{{$order}}</h3>
            
            <p>Orders Waiting</p>
        </div>
        <div class="icon">
            <i class="ion ion-bag"></i>
        </div>
        <a href="/orders" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
</div>
<!-- ./col -->
<div class="col-lg-3 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-yellow">
        <div class="inner">
            <h3>{{$customer}}</h3>
            <p>User Registrations</p>
        </div>
        <div class="icon">
            <i class="ion ion-person-add"></i>
        </div>
        <a href="/customers" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
</div>
@endsection
