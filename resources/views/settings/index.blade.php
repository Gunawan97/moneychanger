@extends('layouts.app')

@section('title')
Settings
@endsection

@section('content')
<div class="row">
	<section class="col-md-12">
        <div class="box box-primary">
            {!! Form::model($setting, ['route' => ['settings.update', $setting[0]->id], 'method'=>'PATCH','enctype'=>'multipart/form-data', 'role' => 'form']) !!}
            <div class="box-body">
                <div class="row">
                    <div class="form-group col-md-3">
                        <label>Bank Account Name</label>
                        {!! Form::text('account_name', $setting[4]->value, ['class'=> 'form-control','required' => 'required']) !!}
                    </div>
                    <div class="form-group col-md-3">
                        <label>Bank Account Number</label>
                        {!! Form::number('account_number', $setting[3]->value, ['class'=> 'form-control','required' => 'required']) !!}
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-3">
                        <label>Default Maximum Transaction</label>
                        {!! Form::text('max_transaction', $setting[5]->value, ['class'=> 'form-control','required' => 'required']) !!}
                    </div>
                    <div class="form-group col-md-3">
                        <label>Payment Expired Time (minute)</label>
                        {!! Form::number('expiredtime', $setting[0]->value, ['class'=> 'form-control','required' => 'required']) !!}
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-3">
                        <div class="bootstrap-timepicker">
                            <div class="form-group">
                                <label>Open Hour</label>
                                
                                <div class="input-group">
                                    {!! Form::text('opentime', $open, ['class'=> 'form-control timepicker','required' => 'required']) !!}
                                    <div class="input-group-addon">
                                        <i class="fa fa-clock-o"></i>
                                    </div>
                                </div>
                                <!-- /.input group -->
                            </div>
                            <!-- /.form group -->
                        </div>
                    </div>
                    <div class="form-group col-md-3">
                        <div class="bootstrap-timepicker">
                            <div class="form-group">
                                <label>Close Hour</label>
                                
                                <div class="input-group">
                                    {!! Form::text('closetime', $close, ['class'=> 'form-control timepicker','required' => 'required']) !!}
                                    <div class="input-group-addon">
                                        <i class="fa fa-clock-o"></i>
                                    </div>
                                </div>
                                <!-- /.input group -->
                            </div>
                            <!-- /.form group -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript">
        $(function () {
            $('.timepicker').timepicker({
                showInputs: false,
            });
            $('.timepicker').keypress(function(event) {
                event.preventDefault();
                return false;
            });
        });
    </script>
    @stop
    
    