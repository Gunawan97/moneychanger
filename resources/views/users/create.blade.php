@extends('layouts.app')

@section('title')
{{ $user->exists ? 'Ubah' : 'Tambah' }} User
@endsection
@section('content')
@if($user->exists)
@section('actionbtn')
<a data-href="{{ route('users.destroy', ['user'=>$user->id]) }}" class="btn btn-danger destroy">Hapus User</a>
@endsection
@endif

<section class="form-candidate">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <!-- /.box-header -->
        <!-- form start -->
      @if($user->exists)
      {!! Form::model($user, ['route' => ['users.update', $user->id], 'method'=>'PATCH','enctype'=>'multipart/form-data', 'role' => 'form']) !!}
      @else
      {!! Form::model($user, ['route' => ['users.store'], 'method'=>'POST','enctype'=>'multipart/form-data', 'role' => 'form']) !!}
      @endif
      <div class="box-body">
        <div class="row">
          <div class="form-group col-md-12">
            <label>Nama</label>
            {!! Form::text('name', $user->name, ['class'=> 'form-control','required' => 'required']) !!}
          </div>
          <div class="form-group col-md-12">
            <label>Email</label>
            {!! Form::text('email', $user->email, ['class'=> 'form-control','required' => 'required']) !!}
            @if ($errors->has('email'))
              <div class="help-block text-red">
                {{ $errors->first('email') }}
              </div>
            @endif
          </div>
          <div class="form-group col-md-12">
            <label>Password</label>
            {!! Form::password('password', ['class'=> 'form-control','required' => 'required']) !!}
            @if ($errors->has('password'))
              <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
              </span>
            @endif
          </div>
          <div class="form-group col-md-12">
            <label>Confirm Password</label>
            {!! Form::password('password_confirmation', ['class'=> 'form-control','required' => 'required']) !!}
          </div>
          <div class="form-group col-md-12">
            <label>Role</label>
            {!! Form::select('role', ['1' => 'Admin', '2' => 'Staff'], $user->role, ['placeholder'=>'Role','class'=> 'form-control select2','required' => 'required']) !!}
          </div>
        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer">
        <div class="col-md-12">
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </div>
    </form>
  </div>
</div>
</div>
</section>
@endsection
@include('layouts._deletebtn')