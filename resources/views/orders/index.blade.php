@extends('layouts.app')

@section('title')
View Order
@endsection

@section('content')

<div class="row" style="margin-bottom:2vh;">
	<!-- Search User -->
	<form method="get" id="search-form">
		<div class="col-md-3">
			<div class="input-group no-border">
				<input type="date" class="form-control column-filter" name="filter[date]" placeholder="Start: " value="{{ !empty($filter['date']) ? $filter['date'] : '' }}"/>
				<span class="input-group-addon icon-search"><i class="fa fa-search"></i></span>
			</div>
			
		</div>
		<div class="col-md-3">
			<div class="input-group no-border">
				{!! Form::select('filter[customer_id]', $customer, null, ['placeholder'=>'Nama','class'=> 'form-control']) !!}
				<span class="input-group-addon icon-search"><i class="fa fa-search"></i></span>
			</div>
		</div>

		<div class="col-md-2">
			<div class="input-group no-border">
				<input type="text" class="form-control column-filter" name="filter[total]" placeholder="Total" value="{{ !empty($filter['total']) ? $filter['total'] : '' }}"/>
				<span class="input-group-addon icon-search"><i class="fa fa-search"></i></span>
			</div>
		</div>

		<div class="col-md-2">
			<div class="input-group no-border">
				<select name="filter[status]" class="form-control column-filter">
					@if(!empty($filter['status']))
					<option value="0" {{ ($filter['status'])==0 ? 'selected' : '' }}>Semua Status</option>
					<option value="1" {{ ($filter['status'])==1 ? 'selected' : '' }}>Order</option>
					<option value="2" {{ ($filter['status'])==2 ? 'selected' : '' }}>Proses</option>
					<option value="3" {{ ($filter['status'])==3 ? 'selected' : '' }}>Dikirim</option>
					<option value="4" {{ ($filter['status'])==4 ? 'selected' : '' }}>Diterima</option>
					@else
					<option value="0" >Semua Status</option>
					<option value="1" >Order</option>
					<option value="2" >Proses</option>
					<option value="3" >Dikirim</option>
					<option value="4" >Diterima</option>
					@endif
				</select>
				<span class="input-group-addon icon-search"><i class="fa fa-search"></i></span>
			</div>
		</div>
		<div class="col-md-2">
			<button type="submit" class="btn btn-primary btn-block">Cari</button>
		</div>
	</form>
</div>
<div class="box">
	<div class="box-header">
		<div class="row">
			
		</div>
	</div>
	<!-- /.box-header -->
	<div class="box-body">
		<table id="example1" class="table table-bordered table-striped">
			<thead>
				<tr>
					<th>No</th>
					<th>Tanggal</th>
					<th>Nama</th>
					<th>Alamat</th>
					<th>Tipe Pengiriman</th>
					<th>Total</th>
					<th>Status</th>
					<th>Show Detail</th>
				</tr>
			</thead>
			<tbody>
				@foreach($order as $c)
				<tr>
					<td>{{$c->id}}</td>
					<td>{{$c->date}}</td>
					<td>{{$c->customer->name}}</td>
					<td>{{$c->customer->address}}</td>
					<td>{{$c->delivery_name}}</td>
					<td>{{$c->total_name}}</td>
					<td style="vertical-align:middle;">
						<center>
							@if($c->status==1)
							<span class="label label-danger" style="font-size:12px;font-weight:400;">
								@elseif($c->status==2)
								<span class="label label-warning" style="font-size:12px;font-weight:400;">
									@elseif($c->status==3)
									<span class="label label-success" style="font-size:12px;font-weight:400;">
										@elseif($c->status==4)
										<span class="label label-primary" style="font-size:12px;font-weight:400;">
											@endif
											{{$c->status_name}}
										</span>
										@if($c->status==1)
										<br>
										{{$c->created_at->addMinutes($expired->value)->diffForHumans(null,true)}}
										@endif
									</center>
								</td>
								<td>
									<a href="{{route('orders.show',$c->id)}}">
										<button class="btn btn-primary">
											Show
										</button>
									</a>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
					@php
					$filters = [];
					foreach ($filter as $key => $value) {
					$filters['filter['. $key .']'] = $value;
				}
				@endphp
				{{ $order->appends($filters)->links() }}
			</div>
		</div>
		@stop
		@push('scripts')

		<script>
			$(".select2").select2();
		</script>
		@endpush