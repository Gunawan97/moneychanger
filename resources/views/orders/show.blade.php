@extends('layouts.app')
@push('styles')
<style>
#map {
 width: 100%;
 height: 400px;
 background-color: grey;
}
p{
 font-size: 18px;
 font-weight: bold;
}
</style>
@endpush
@section('title')
Detail Order
@endsection

@section('content')
<div class="row">
  <section class="col-md-12" >
    <div class="box">
      <div class="box-header">
        <div class="row">
          <div class="col-md-12" >


           @if($order->status == 1)
           <div class="col-md-5">
            <b style="color:red">
             {{ $order->created_at->addMinutes($expired->value)->format('d/m/Y H:i:s') }}
             <br>
             {{ $order->created_at->addMinutes($expired->value)->diffForHumans(null,false) }}
           </b>
         </div>
         {!! Form::model($order, ['route' => ['orders.update', $order->id],'method'=>'PATCH', 'role' => 'form']) !!}
         <button type="submit" class="btn btn-primary col-md-2 pull-right" style="margin:8px;">Proses</button>
       </form>
       @elseif($order->status == 2 && $order->delivery == 1 )
       <button class="btn btn-primary col-md-2 pull-right"  style="margin:8px;" data-hover="tooltip" data-placement="top" data-target="#editModal" data-toggle="modal" id="modal-edit">Kirim</button>
       @elseif($order->status == 3 && $order->delivery == 2 )
       <button class="btn btn-primary col-md-2 pull-right"  style="margin:8px;" data-hover="tooltip" data-placement="top" data-target="#editModal2" data-toggle="modal" id="modal-edit">Diambil</button>
       @elseif($order->status == 3)
       <button class="btn btn-primary col-md-2 pull-right"  style="margin:8px;" data-hover="tooltip" data-placement="top" data-target="#editModal2" data-toggle="modal" id="modal-edit">Diterima</button>
       @endif
       @if($order->status < 4)
       {!! Form::model($order, ['route' => ['orders.update', $order->id],'method'=>'PATCH', 'role' => 'form']) !!}
       <button type="submit" name="cancel" value="1" class="btn btn-danger col-md-2 pull-right" style="margin:8px;">Batal</button>
     </form>
     @endif
               @if($order->status < 3 )
          {!! Form::model($order, ['route' => ['orders.update_delivery', $order->id],'method'=>'PATCH', 'role' => 'form']) !!}
          <button type="submit" class="btn btn-warning col-md-2 pull-right " style="margin:8px;">Ganti Pengiriman</button>
          {!! Form::close() !!} 

          @endif
   </div>
 </div>
</div>
<div class="box-body">
  <div class="col-md-4">
    <div class="row">
      <div class="col-md-12" >
        <label>Tanggal Order</label>
        <p> {{$order->date}}</p>
      </div>
      <div class="col-md-12" >
        <label>Nama Customer</label>
        <p> {{$order->customer->name}}</p>
      </div>
    </div>
  </div>
  <div class="col-md-4">
    <div class="row">
      <div class="col-md-12" >
        <label>Alamat Pengiriman</label>
        <p> {{$order->customer->address}}</p>
      </div>
      <div class="col-md-12" >
        <label>Nama Kurir</label>
        @if($order->courier_id != NULL)
        <p><a href="{{route('couriers.show',$order->courier_id)}}"> {{$order->courier->username}}</a></p>
        @else
        <p>-</p>
        @endif
      </div>
    </div>
  </div>
  <div class="col-md-4">
    <div class="row">
      <div class="col-md-12" >
        <label>Status Pengiriman</label>
        <p> {{$order->status_name}}</p>
      </div>
      <div class="col-md-12" >
        <label>Tipe Pengiriman</label>

        <p>

          {{$order->delivery_name}}

        </p>
      </div>
    </div>
  </div>
</div>	
</div>
</section>

<section class="col-md-12" >
  <div class="box">
    <div class="box-body">
      <div class="row">
        <div class="col-md-12" >
          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
               <th>ID</th>
               <th>Nominal</th>
               <th>Jumlah</th>
               <th>Harga Satuan</th>
               <th>Sub Total</th>
             </tr>
           </thead>
           <tbody>
            @foreach($order->detail_order as $c)
            <tr>
             <td>{{$c->id}}</td>
             <td>{{$c->currency_log->detail_currency->currency->code}} {{$c->currency_log->detail_currency->nominal_name}}</td>
             <td>{{$c->amount}}</td>
             <td>{{$c->price_name}}</td>
             <td>{{$c->total_name}}</td>
           </tr>
           @endforeach
         </tbody>
       </table>
     </div>
   </div>
 </div>	
 <div class="box-footer">
  <div class="col-md-2 col-md-offset-7">
    <p>TOTAL</p>
  </div>
  <div class="col-md-3">
    <p>{{$order->total_name}}</p>
  </div>

</div>
</div>
</section>
<section class="col-md-6">
  @foreach($order->history_change as $h)
  <div class="box">
    <div class="box-header">
      <div class="row">
        <div class="col-md-12">
          <label>{{$h->date}} oleh {{$h->user}}</label>
          <p>{{$h->comment}}</p>
        </div>
      </div>
    </div>  
  </div>
  @endforeach
</section>
@if($order->receiver_name != NULL)
<section class="col-md-6">
  <div class="box">
    <div class="box-header">
      <div class="row">
        <div class="col-md-12">
          @if($order->latitude != null)
          <div id="map"></div>
          @endif
        </div>

        <div class="col-md-6">
          <label>Nama Penerima</label>
          <p>{{$order->receiver_name}}</p>
        </div>

        <div class="col-md-6">
          @if($order->image_receipt != NULL)
          <div class="row">
            <label>Tanda Tangan</label>
          </div>
          
          <img src="data:image/jpg;base64,{{$order->image_receipt}}" height="200px">
          @endif
        </div>
      </div>
    </div>	
  </div>
</section>
@endif
<!--EDIT MODAL-->
<div class="modal modal-warning fade" id="editModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Pilih Kurir</h4>
        </div>

        {!! Form::model($order, ['route' => ['orders.update', $order->id], 'method'=>'PATCH', 'role' => 'form']) !!}
        <div class="modal-body col-sm-12">
          <div class="form group col-sm-12">
            <label>Nama Kurir</label> 
            {!! Form::select('courier_id',$courier, NULL,['class'=> 'form-control','required' => 'required']) !!}

          </div>
        </div>
        <div class="modal-footer col-sm-12">
          <button type="submit" class="btn btn-warning pull-right"> Send</button>
        </div>
      </form>
    </div>
  </div>
</div>
<!--END EDIT MODAL-->
<!--EDIT MODAL-->
<div class="modal modal-primary fade" id="editModal2">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal2" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Penerima</h4>
        </div>

        {!! Form::model($order, ['route' => ['orders.update', $order->id], 'method'=>'PATCH', 'role' => 'form']) !!}
        <div class="modal-body col-sm-12">
          <div class="form group col-sm-12">
            <label>Nama Penerima</label>   
            {!! Form::text('receiver_name', null, ['class'=> 'form-control','required' => 'required']) !!}



          </div>
        </div>
        <div class="modal-footer col-sm-12">
          <button type="submit" class="btn btn-warning pull-right"> Send</button>
        </div>
      </form>
    </div>
  </div>
</div>
<!--END EDIT MODAL-->
</div>
@stop

@push('scripts')
<script>
  function initMap() {
    var latitude = <?php echo json_encode($order->latitude); ?>;
    var longitude = <?php echo json_encode($order->longitude); ?>;
    var uluru = {lat: latitude, lng: longitude};
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 100,
      center: uluru
    });
    var marker = new google.maps.Marker({
      position: uluru,
      map: map
    });
  }
</script>
<script async defer
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBX2xpTl4M-x7QOH-KkHQDpZQFY0xxgdT8&callback=initMap">
</script>
@endpush