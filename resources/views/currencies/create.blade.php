@extends('layouts.app')

@section('title')
{{ $currency->exists ? 'Ubah' : 'Tambah' }} Mata Uang
@endsection
@section('content')

<section class="form-candidate">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <!-- /.box-header -->
        <!-- form start -->
      @if($currency->exists)
      {!! Form::model($currency, ['route' => ['currencies.update', $currency->id], 'method'=>'PATCH','enctype'=>'multipart/form-data', 'role' => 'form']) !!}
      @else
      {!! Form::model($currency, ['route' => ['currencies.store'], 'method'=>'POST','enctype'=>'multipart/form-data', 'role' => 'form']) !!}
      @endif
      <div class="box-body">
        <div class="row">
          <div class="form-group col-md-12">
            <label>Nama</label>
            {!! Form::text('name', $currency->name, ['class'=> 'form-control','required' => 'required']) !!}
          </div>
          <div class="form-group col-md-12">
            <label>Kode</label>
            {!! Form::text('code', $currency->code, ['class'=> 'form-control','required' => 'required']) !!}
          </div>
        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer">
        <div class="col-md-12">
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </div>
    </form>
  </div>
</div>
</div>
</section>
@endsection