@extends('layouts.app')

@section('title')
Manage Mata Uang
@endsection

@section('content')

<div class="row" style="margin-bottom:2vh;">
	<!-- Search User -->
	<form method="get" id="search-form">
		<div class="col-md-3">
			<div class="input-group no-border">
				<input type="text" class="form-control column-filter" name="filter[name]" placeholder="Nama" value="{{ !empty($filter['name']) ? $filter['name'] : '' }}"/>
				<span class="input-group-addon icon-search"><i class="fa fa-search"></i></span>
			</div>
		</div>
		<div class="col-md-3">
			<button type="submit" class="btn btn-primary btn-block">Cari</button>
		</div>
	</form>
</div>
<div class="box">
	<div class="box-header">
		<div class="row">
			<div class="col-md-3">
				<a href="{{ route('currencies.create')}}" class="btn btn-success btn-block">Tambah Mata Uang &nbsp;<span class="fa fa-plus-circle"></span></a>
			</div>
		</div>
	</div>
	
	<!-- /.box-header -->
	<div class="box-body">
		<table id="example1" class="table table-bordered table-striped">
			<thead>
				<tr>
					<th>ID</th>
					<th>Kode</th>
					<th>Nama</th>
					<th>Show Detail</th>
				</tr>
			</thead>
			<tbody>
				@foreach($currency as $c)
				<tr>
					<td>{{$c->id}}</td>
					<td>{{$c->code}}</td>
					<td>{{$c->name}}</td>
					<td>
						<a href="{{route('currencies.show',$c->id)}}">
							<button class="btn btn-primary">
								Show
							</button>
						</a>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
		@php
		$filters = [];
		foreach ($filter as $key => $value) {
		$filters['filter['. $key .']'] = $value;
	}
	@endphp
	{{ $currency->appends($filters)->links() }}
</div>
</div>

@stop
