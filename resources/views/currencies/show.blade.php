@extends('layouts.app')

@section('title')
Manage Pecahan Mata Uang
@endsection

@section('content')
<div class="row">
    <section class="col-md-12">
        <div class="box">
            <div class="box-header">
            <div class="row">
            	<div class="col-md-12" >
                	<a href="{{route('currencies.edit',$currency->id)}}">
						<button class="btn btn-warning col-md-2 pull-right">
							Edit
						</button>
					</a>
            	</div>
           </div>
       </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-4">
                    <label>Mata Uang</label>
                    <p>{{$currency->name}}</p>
                </div>
            </div>
        </div>
    </section>
	<section class="col-md-12">
		<div class="box">
			
			<div class="box-header">
				<div class="row">
					<div class="col-md-4">
						<h3>Pecahan Mata Uang</h3>
					</div>
					<div class="col-md-4 pull-right">
						<a href="{{ route('currencies.detail_currencies.create',$currency->id)}}" class="btn btn-success btn-block">Tambah Pecahan Mata Uang &nbsp;<span class="fa fa-plus-circle"></span></a>
					</div>
				</div>
			</div>	
			<div class="box-body" style="height: 300px;overflow-y:visible" >
				<table id="example1" class="table table-bordered table-striped">
					<thead>
						<tr>
							<th>Nominal</th>
							<th>Edit</th>
						</tr>
					</thead>
					<tbody>
						@foreach($detail_currency as $a)
						<tr> 
							<td>{{ $currency->name.' '.$a->nominal }}</td>
							<td>
								<a href="{{route('currencies.detail_currencies.edit',['currency'=>$currency->id,'detail_currency'=>$a->id])}}">
									<button class="btn btn-warning">
										Edit
									</button>
								</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
				{{ $detail_currency->links() }}
			</div>
		</div>
	</section>
</div>
@stop