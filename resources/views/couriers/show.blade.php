    @extends('layouts.app')
    @section('title')
    Detail Kurir
    @endsection
    
    @section('content')
    <div class="row">
        <section class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <div class="row">
                        <div class="col-md-12" >
                        {!! Form::model($courier, ['route' => ['couriers.destroy', $courier->id],'method'=>'DELETE', 'role' => 'form']) !!}
                        <button type="submit" style="margin:8px;" class="btn btn-danger col-md-2 pull-right"> Delete</button>
                        </form>
                        {!! Form::model($courier, ['route' => ['couriers.update', $courier->id],'method'=>'PATCH', 'role' => 'form']) !!}
                        
                        @if($courier->status==1)
                        <button type="submit" style="margin:8px;" class="btn btn-warning col-md-2 pull-right"> Suspend</button>
                        @else
                        <button type="submit" style="margin:8px;" class="btn btn-primary col-md-2 pull-right"> Approve</button>
                        @endif
                    </form>
                    
                </div>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-4">
                    <label>Nama</label>
                    <p>{{$courier->username}}</p>
                </div>
                <div class="col-md-4">
                    <label>IMEI</label>
                    <p>{{$courier->imei}}</p>
                </div>
                <div class="col-md-4">
                    <label>Status</label>
                    <p>{{$courier->status_name}}</p>
                </div>
            </div>
        </div>	
    </div>
</section>


<section class="col-md-12">
    <div class="box">
        <div class="box-header">
            <div class="row">
                <div class="col-md-3">
                    History Pengiriman
                </div>
            </div>
        </div>
        
        <!-- /.box-header -->
        <div class="box-body" style="height: 65vh;overflow-y:visible">
            <div class="row">
                <div class="col-md-12" style="height: 400px;">
                    <table id="example1" class="table table-bordered table-striped " >
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Customer</th>
                                <th>Total</th>
                                <th>Tanggal</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($order as $key => $a)
                            <tr> 
                                <td>{{ $key+1}}</td>
                                <td>{{ $a->customer->name}}</td>
                                <td>IDR {{ $a->total }}</td>
                                <td>{{ $a->created_at }}</td>
                                <td>{{ $a->status_name }}</td>
                                <td>
                                    <a href="{{route('orders.show',$a->id)}}" class="btn btn-primary">
                                        Show Order
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    
                    {{ $order->links() }}
                </div>
            </div>
        </div>
    </div>
</section>
</div>
@stop