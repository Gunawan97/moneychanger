@extends('layouts.app')

@section('title')
{{ $detail_currency->exists ? 'Ubah' : 'Tambah' }} Pecahan Mata Uang
@endsection
@section('content')
@if($detail_currency->exists)
@section('actionbtn')
<a data-href="{{ route('currencies.detail_currencies.destroy', ['currency'=>$currency->id,'detail_currency'=>$detail_currency->id]) }}" class="btn btn-danger destroy">Hapus Pecahan Mata Uang</a>
@endsection
@endif

<section class="form-candidate">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <!-- /.box-header -->
        <!-- form start -->
        @if($detail_currency->exists)
        {!! Form::model($detail_currency, ['route' => ['currencies.detail_currencies.update', 'currency'=>$currency->id, 'detail_currency'=>$detail_currency->id], 'method'=>'PATCH','role' => 'form']) !!}
        @else
        {!! Form::model($detail_currency, ['route' => ['currencies.detail_currencies.store',$currency->id], 'method'=>'POST', 'role' => 'form']) !!}
        @endif
        <div class="box-body">
          <div class="row">
            <div class="form-group col-md-12">
              <label>Nominal</label>
              {!! Form::number('nominal',$detail_currency->nominal,['class'=> 'form-control','require'=>'required']) !!}
              @if ($errors->any())
              <div class="alert alert-danger">
                <ul>
                  @foreach ($errors->all() as $error)
                      {{ $error }}
                  @endforeach
                </ul>
              </div>
              @endif
            </div>
          </div>
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>
</section>
@endsection
@include('layouts._deletebtn')