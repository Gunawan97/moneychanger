<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        DB::table('users')->insert([ 
            'name' => 'admin',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('secret'),
            'role' => 1
            ]);
        DB::table('users')->insert([ 
            'name' => 'staff',
            'email' => 'staff@gmail.com',
            'password' => bcrypt('secret'),
            'role' => 2
            ]);
        }
    }
    