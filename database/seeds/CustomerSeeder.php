<?php

use Illuminate\Database\Seeder;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sets = [['Arman','01','1','123','1.jpg','Jeruk',1000000,1],['Bima','02','2','456','2.jpg','Apel',1000000,2],['Calvin','03','3','789','3.jpg','Pir',1000000,3]];
        foreach($sets as $set){
            DB::table('customers')->insert([
                'name' => $set[0],
                'imei' => $set[1],
                'card_id' => $set[2],
                'phone' => $set[3],
                'card_image' => $set[4],
                'address' => $set[5],
                'max_transaction' => $set[6],
                'status' => $set[7]
                ]);
        }
    }
}
