<?php

use Illuminate\Database\Seeder;

class CourierSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sets = [['01','Andi',2],['02','Budi',1],['03','Charlie',1]];
        foreach($sets as $set){
            DB::table('couriers')->insert([ 
                'imei' => $set[0],
                'username' => $set[1],
                'status' => $set[2]
                ]);
        }
    }
}
