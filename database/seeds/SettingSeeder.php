<?php

use Illuminate\Database\Seeder;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sets = [['expiredtime','30'],['opentime','08:00 AM'],['closetime','04:00 PM'],['account_number','12345678'],['account_name','admin'],['max_transaction','1000000']];
        foreach($sets as $set){
            DB::table('settings')->insert([ 
                'name' => $set[0],
                'value' => $set[1]
                ]);
        }
    }
}
