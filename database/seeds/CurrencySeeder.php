<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class CurrencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sets = [['SGD','Singapore Dollar'],['AUD','Australian Dollar'],['ASD','American Dollar']];
        foreach($sets as $set){
            DB::table('currencies')->insert([ 
                'code' => $set[0],
                'name' => $set[1]
                ]);
        }
        $sets = [['1',50],['1',100],['2',50],['2',100],['3',50],['3',100]];
        foreach($sets as $set){
            DB::table('detail_currencies')->insert([ 
                'currency_id' => $set[0],
                'nominal' => $set[1],
                ]);
        }

        $sets = [['1',50,100,10000],['2',100,100,9900],['3',50,100,10500],['4',100,100,10400],['5',50,100,13000],['6',100,100,12900]];
        foreach($sets as $set){
            DB::table('currency_logs')->insert([ 
                'detail_currency_id' => $set[0],
                'stock' => $set[1],
                'price' => $set[1],
                ]);
        }

        $sets = [['Arman','01','1','123','1.jpg','Jeruk',1],['Bima','02','2','456','2.jpg','Apel',2],['Calvin','03','3','789','3.jpg','Pir',3]];
        $date = Carbon::now();
        foreach($sets as $set){
            DB::table('orders')->insert([
                'order_number' => $set[1],
                'customer_id' => $set[2],
                'total' => $set[3],
                'delivery' => 1,
                'status' => 1,
                'created_at' => $date
                ]);
        }
        foreach($sets as $set){
            DB::table('detail_orders')->insert([
                'order_id' => $set[2],
                'currency_log_id' => 1,
                'amount' => 1,
                'price' => 1,
                'total' => 1,
                ]);
        }
    }
}
